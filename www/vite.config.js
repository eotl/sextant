import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import copy from 'rollup-plugin-copy'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        copy({
            targets: [
                // Not sure if this is the best solution, but the files in /dist are not served in development so you can't read them. But now you don't see a difference between the coppied files and the local ones
                // Maybe we need to make an extra folder in like `external` or something that shows these files are being coppied there...
                { src: path.resolve(__dirname, "node_modules/leaflet/dist/images/*"), dest: './public/images' },
                { src: path.resolve(__dirname, "node_modules/@eotl/icons/dist/*"), dest: './public/images/icons' },
                { src: path.resolve(__dirname, "node_modules/@eotl/theme-bootstrap/dist/fonts/*"), dest: './public/fonts' },
                { src: path.resolve(__dirname, "node_modules/@eotl/theme-bootstrap/dist/js/*"), dest: './public/js' },
            ],
        }),
        vue()
    ],
    build: {
        outDir: 'dist',
        emptyOutDir: true,
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
        extensions: ['.js', '.vue']
    },
    server: {
        port: 8086,
        host: '0.0.0.0',
        build: {
            outDir: 'dist/',
            sourcemap: true,
            chunkSizeWarningLimit: 25000,
            rollupOptions: {
                output: {
                    manualChunks: {
                        'libs': ['vue'],
                    },
                },
            },
        },
    }
})
