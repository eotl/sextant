import { createRouter, createWebHashHistory } from 'vue-router'

const DEFAULT_TITLE = 'Sextant'

const routes = [{
    path: '/',
    name: 'mapper',
    component: () => import('@/views/Mapper.vue'),
    meta: { title: 'Mapper' }
}, {
    path: '/places',
    name: 'places',
    component: () => import('@/views/Places.vue'),
    meta: { title: 'Places' }
}, {
    path: '/tours',
    name: 'tours',
    component: () => import('@/views/Tours.vue'),
    meta: { title: 'Tours' }
}, {
    path: '/tour/:id',
    name: 'tour-detailpage',
    component: () => import('@/views/Tour.vue'),
    meta: { title: 'Tour' }
}, {
    path: '/zones',
    name: 'zones',
    component: () => import('@/views/Zones.vue'),
    meta: { title: 'Zones' }
}]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

router.afterEach((to) => {
    document.title = to.meta.title + ' : ' + DEFAULT_TITLE || DEFAULT_TITLE
})

export default router

