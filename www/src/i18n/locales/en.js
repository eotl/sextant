const messages = {
    welcome: "Welcome to Sextant",
    tour: {
        build: "Tour Builder", 
        buildMessage: "Add from one or all of the following:",
        buildAdd1: "Saved places & contacts",
        buildAdd2: "New addresses & coordinates",
        buildAdd3: "Upload CSV of addresses"
    }
}

export default messages;
