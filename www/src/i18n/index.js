import _ from 'lodash'
import { createI18n } from 'vue-i18n'

import { en, de } from '@eotl/core/locales'
import enSextant from './locales/en.js'
import deSextant from './locales/de.js'

const enMerge = _.merge(en, enSextant)
const deMerge = _.merge(de, deSextant)

const messages = {
    en: enMerge,
    de: deMerge,
}

const i18n = createI18n({
    locale: "en",
    allowComposition: true,
    messages,
    silentFallbackWarn: true
})

export default i18n
