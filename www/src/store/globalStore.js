import { defineStore } from 'pinia'
import { useEotlCore } from '@eotl/core/store';

export const useStoreGlobal = defineStore({
    id: 'main',
    state: () => ({
        config: {
            mapboxToken: '',
            url_sextant: import.meta.env.VITE_SEXTANT_URL,
            url_api: import.meta.env.VITE_SEXTANT_API_URL,
            api_ending: "",
            path_icons: "/images/icons/",
            icons_map: {
                riders: 'bicycle-1.svg',
                suppliers: 'supplier.svg',
                customers: 'geopoint.svg',
                contacts: 'places.svg'
            },
            lang: "en",
            city: "Berlin",
            postal: "10178",
            country: "DE",
            group: "contacts",
            levels: ["admin"],
            zone_parent: "BRLN"
        },
        user: {
            role: null,
            location: null
        },
        groups: [],
        riders: [],
        tours: [],
        zones: {},
        selectedTour: {
            id: 0,
            client: 'none',
            name: 'Tour Name',
            slug: 'tour-name',
            date: "2021-01-01",
            deliveries: [],
            pickups: [],
            riders: [],
            pickup_time: {
                "start": "12:30",
                "end": "14:30"
            },
            count_stops: 1,
            count_cargo: 1,
            zones: ["ber"]
        },
        alert: {
            show: false,
            style: 'primary',
            title: 'Hello',
            message: 'This is just a default message, nothing to see here!',
        },
        count: 0, // Assuming this is part of the state, as it's used in mutations
    }),
    getters: {
        evenOrOdd: (state) => state.count % 2 === 0 ? 'even' : 'odd',
        getIcon: (state) => (iconKey) => {
            const iconName = state.config.icons_map[iconKey]
            return `${state.config.path_icons}${iconName}`
        }
    },
    actions: {
        alertMsg(alert) {
            this.alert.message = alert.message
            this.alert.show = alert.show
            this.alert.style = alert.style
            this.alert.title = alert.title
        },
        async updateTourStatus() {
            const eotlStore = useEotlCore()
            try {
                const response = await fetch(this.config.url_api + '/tours/' + this.selectedTour.slug)
                if (!response.ok)
                    throw `Error response code: ${response.status}`
                const tour = await response.json()
                this.$patch((state) => {
                    state.selectedTour.deliveries = tour?.deliveries
                })
            } catch (error) {
                eotlStore.alertWarning(error, 'Updating status failed')
                throw error
            }
        },
        // Fetch logic
        async fetchGroups() {
            const eotlStore = useEotlCore()
            try {
                const response = await fetch(this.config.url_api + '/groups')
                if (!response.ok) throw new Error('Something went wrong.')

                const data = await response.json()
                // Directly merge the data into the state
                Object.assign(this, data)

            } catch (error) {
                eotlStore.alertWarning(error, 'Fetching groups failed')
                throw error
            }
        },
        async fetchTours() {
            const eotlStore = useEotlCore()
            const { tours } = await eotlStore.fetchGet('/tours')
            this.$patch((state) => {
                state.tours = tours
            })
        },
        async fetchTourItem(id) {
            const eotlStore = useEotlCore()
            try {
                const response = await fetch(this.config.url_api + '/tours/' + id)
                if (!response.ok)
                    throw `Error response code: ${response.status}`
                const tour = await response.json()
                this.$patch((state) => {
                    state.selectedTour = tour
                })
            } catch (error) {
                eotlStore.alertWarning(error, 'Fetching tour failed')
                throw error
            }
        },
        async fetchRiders() {
            const eotlStore = useEotlCore()
            const { contacts } = await eotlStore.fetchGet('/contacts', '?group=riders')
            this.$patch((state) => {
                state.riders = contacts
            })
        },
        async fetchZones() {
            const response = await fetch(this.config.url_api + '/zones?id=' + this.config.zone_parent)
            const { features } = await response.json()
            if (features) {
                // Update Store
                //geojson.features = features
                this.$patch((state) => {
                    state.zones = features
                })
            }
        }
    }
})
