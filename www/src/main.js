import { createPinia } from 'pinia'
import { createApp } from 'vue'
import { useEotlCore } from '@eotl/core/store'

import App from './App.vue'
import router from './router'
import jQuery from 'jquery'
import i18n from './i18n'
import 'leaflet/dist/leaflet.css'
import '@eotl/theme-bootstrap/dist/css/eotl.css'
import '@eotl/theme-bootstrap/dist/css/styleguide.css'
import '@eotl/core/dist/style.css'

window.global ||= window
window.jQuery = jQuery
window.$ = jQuery

const pinia = createPinia()
const app = createApp(App)
// register Pinia store
app.use(pinia)

//Install i18n instance
app.use(i18n)

// register vue router
app.use(router)

app.mount('#sextant')

// init EOTL core
const eotlStore = useEotlCore()
eotlStore.init({
    // mapboxToken: '',
    url_sextant: import.meta.env.VITE_SEXTANT_URL,
    url_api: import.meta.env.VITE_SEXTANT_API_URL,
})

