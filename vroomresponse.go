package sextant

type VroomResponse struct {
	Code   int          `json:"code"`
	Error  string       `json:"error"`
	Routes []VroomRoute `json:"routes"`
}

func (r *VroomResponse) GetRoutes() []VroomRoute {
	routes := []VroomRoute{}
	for _, route := range r.Routes {
		routes = append(routes, route)
	}
	return routes
}

type VroomRoute struct {
	VehicleID int                `json:"vehicle"`
	Steps     []VroomRoutingStep `json:"steps"`
}

type VroomRoutingStep struct {
	Type        VroomRoutingStepType `json:"type"`
	Description string               `json:"description"`
	Location    *[2]float64          `json:"location"`
	ID          int                  `json:"id"`
}

type VroomRoutingStepType string

const (
	START    VroomRoutingStepType = "start"
	JOB                           = "job"
	PICKUP                        = "pickup"
	DELIVERY                      = "delivery"
	BREAK                         = "break"
	END                           = "end"
)
