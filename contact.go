package sextant

import (
	"context"
	"time"
)

type Contact struct {
	ID        int       `json:"-" yaml:"-"`
	Amount    int       `json:"-"`
	Email     string    `json:"email"`
	Name      string    `json:"name"`
	Phone     string    `json:"phone"`
	From      DateTime  `json:"from"`
	To        DateTime  `json:"to"`
	CreatedAt time.Time `json:"-" yaml:"-"`
	UpdatedAt time.Time `json:"-" yaml:"-"`
}

func (p *Contact) TimeWindow() *TimeWindow {
	if p.From.Time.IsZero() || p.To.Time.IsZero() {
		return nil
	}
	return &TimeWindow{p.From.Time, p.To.Time}
}

func (p *Contact) TimeWindows() *[]*TimeWindow {
	tw := p.TimeWindow()
	if tw == nil {
		return nil
	}
	return &([]*TimeWindow{tw})
}

type ContactFilter struct {
	ID     *int    `json:"id"`
	Email  *string `json:"email"`
	Name   *string `json:"name"`
	Phone  *string `json:"phone"`
	Offset int     `json:"offset"`
	Limit  int     `json:"limit"`
}

func (c *Contact) Validate() error {
	return nil
}

type ContactService interface {
	FindOrCreateContact(ctx context.Context, contact *Contact) error

	FindContactByID(ctx context.Context, id int) (*Contact, error)
}
