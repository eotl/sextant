---
title: Deployment
titleFull: Deployment of Sextant
leafs: ["sextant", "cli"]
lang: "English"
---

## Co-op Cloud

> If you're not familiar with Co-op Cloud, see [the docs](https://docs.coopcloud.tech/) first.

See [git.coopcloud.tech/coop-cloud/sextant](https://git.coopcloud.tech/coop-cloud/sextant).

To deploy a new Sextant instance with Abra, you run the following:

```
$ abra app new sextant # requires a configured server
$ abra app config <domain>
```

Presently, the only authentication for *Sextant* is `HTTP Basic Authentication` and to enable that, uncomment these lines from the `.env` file:

```
COMPOSE_FILE="compose.yml:compose.basicauth.yml"
HTTP_BASIC_AUTH_ENABLED=1
HTTP_BASIC_AUTH_USERNAME=foo
SECRET_AUTH_PASSWORD_VERSION=v1
```

Now, generate the HTTP Basic Auth password with the following:

```
$ abra app secret generate -a <domain>
```

Now you're ready to deploy *Sextant* to your server:

```
$ abra app deploy <domain>
```

You should now to be able to login to your *Sextant* instance and it almost works. Once you confirm that it does, proceed to import default `zones` data with the following.

```
$ abra app cmd <domain> app zones
```

**Note**: This iteration of the recipe is still experimental and there is
currently no specific release version. This will change in the Near Future ™
For now, you can deploy the latest recipe commit.


### Backup & restore

Until we add support to the *Co-op Cloud Recipe* this will be currently a manual process, but seeing as it's only copying two files, it is quite easy and fool proof.

#### Backup

```
$ cd backups/
$ abra app cp <domain> app:/home/sextant/.sextant.conf .
$ abra app cp <domain> app:/home/sextant/.sextant.db .
```

#### Restore

First SSH into the running `app` docker container and **delete** to freshly installed config and dB.

```
$ abra app run <domain> app bash
$ cd /home/sextant/
$ rm .sextant.conf
$ rm .sextant.db
```

In another terminal, use `abra app cp` command to copy the files from your backup:

```
$ abra app cp <domain> backups/.sextant.conf app:/home/sextant/
$ abra app cp <domain> backups/.sextant.db app:/home/sextant/
```

Now restart your deployed app with:

```
$ abra app restart <domain> app
```

Then hard-refresh (Ctrl+R) in your browser to clear the cache. Things should be restored.


## Systemd service

Create a service unit for systemd at `/etc/systemd/system/sextantd.service`
with the following

```
[Unit]
Description=Sextant Daemon

[Service]
ExecStart=/home/user/.local/bin/sextantd
Restart=always

[Install]
WantedBy=multi-user.target
```

Don't forget to update the path to the `sextantd` binary.
