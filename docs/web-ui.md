---
title: Web UI
titleFull: Sextant Web UI
kind: documenation
leafs: ["sextant", "web ui"]
---

The Web UI for *Sextant* consists of multiple front-end assets which are kept in
separate repos.

- [`bootstrap-eotl`](https://codeberg.org/eotl/bootstrap-eotl) EOTL's Bootstrap 4 theme
- [`icons`](https://codeberg.org/eotl/icons) EOTL's custom icon set
- [`core`](https://codeberg.org/eotl/core) various Vanilla and Vue.js javascript code used across EOTL apps

The actual *Sextant* Web UI code (Vue3) exists in the `sextant/www` directory of
this repository. The UI build process compiles all of these things.

Everything is handled by a helpful *Makefile* we created.

## First Run

To do this, go into the `www` directory and install JS dependencies with the
`yarn` package manager and build the UI with `vite` run the following series of
commands:

```
$ cd www/
$ make first-run
$ make build
```

Now run `sextantd` in another terminal with commands:

```
$ cd ../
$ ./sextand -v
```

Now you should see the Sextant Web UI available at 

- `localhost:8081`


## Developing

Serving the Web UI for development with hot compiling reload on changes to the
JS code is done with the following helper:

```
$ make dev
```

This serves the Web UI from a `vite` webserver *instead of* the `sextantd` HTTP
server and is exposed through a Docker container to:

- [localhost:8086](http://localhost:8086)


**Modifing local JS dependencies**

When working locally and making changes to the external [`eotl/core`] dependency
you can clone place the repo as a sibling to this repo and add link it to the
`@eotl/core` package in your `package.json` by editing it as:

```
"dependencies": {
    "@eotl/core": "link:../../../eotl/core",
    ...
}
```

Then run `yarn install` to link the module on your local. Be sure to remove this
when needing to commit `package.json` to the repo.

However, this doesn't work if you use the above Docker container Makefile
process. 

### Linting

Make sure you have the linting package installed in your IDE to unify the code styling.


## Production

```
$ make build
```
