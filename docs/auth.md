---
title: Authentication
titleFull: Sextant Authentication
leafs: ["sextant", "auth"]
lang: "English"
---

⚠️ Warning: No Authentication Out of the Box ⚠️

With the current *Sextant* implementation authentication is only existing "out of the box"  with [HTTP Basic Auth](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication).

There is partial support with our experimental [`ssrd`](#ssrd) method, but it requires hacking at it a bit. We are working on something proper [at present](https://codeberg.org/eotl/sextant/issues/76).


## HTTP Basic Authentication

This method is implemented in Go Iris framework and works well enough on a
single user instance or if all users should have access to all the data. To
enable this  following two options are enabled in the `sextant.conf` file:

```toml
http_basic_auth_username = "sextant"
http_basic_auth_password = "sekrit magic bicycle 123"
```

Then HTTP Basic Auth will be enabled for all endpoints.


### SSSRD

There is a scheme around, let's call it a recipe that is missing ingredients,
that adds a URI granular ACL at per request which uses our experimental [Simple
Signed Records](https://codeberg.org/eotl/simple-signed-records) method which
uses ed25519 keys to sign HTTP requests, but the WebUI does not support this at
the moment, so it only works on raw API calls.

This works by running
[ssrd](https://codeberg.org/eotl/simple-signed-records/src/branch/main/README.md#ssrd)
as a proxy server that verifies requests to `sextantd` server. In order to do
this, make sure that is compiled and running correctly, and be sure to edit the
line to your `~/.sextant.conf`

```
ssrauth_filepath = "/var/www/htdocs/default/.ssr/auth"
```

You then need to grant appropriate keys access to the appropriate paths. An
example of this [be found
here](https://codeberg.org/eotl/simple-signed-records/src/branch/main/testing/website).
