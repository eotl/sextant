---
title: Release management
titleFull: Sextant release management
leafs: ["sextant", "cli"]
lang: "English"
---

## Overall steps

- Git: tag and push a new commit
- Docker: build/push the new tag
- GoReleaser: build binaries for the new tag
- Codeberg: Manually create new release [here](https://codeberg.org/eotl/sextant/releases)

## Git

```
git tag -a <tag>
git push && git push --tags
```

## Docker

> [`codeberg.org/eotl/sextant`](https://codeberg.org/eotl/-/packages/container/sextant/latest)

```
export TAG=latest
docker login codeberg.org
docker build -t codeberg.org/eotl/sextant:$TAG .
docker push codeberg.org/eotl/sextant:$TAG
```

## GoReleaser

> You need to run this command on the same commit as the latest release tag.

```
goreleaser build --clean
```

See resulting `./dist` for build files.

### Testing the build

```
goreleaser build --snapshot --single-target --clean
```

## Codeberg

> [Create new release](https://codeberg.org/eotl/sextant/releases/new)

You can write the changelog, upload files, etc. manually.

Choose `Choose an existing tag or create a new tag` and select the tag you just pushed 👍
