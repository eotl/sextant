---
title: Architecture
titleFull: Sextant Architecture
leafs: ["sextant", "architecture", "geocoding", "where39"]
lang: "English"
---

*Sextant* relies on two additional web services which use [OpenStreetMap](https://www.openstreetmap.org) map tiles.


### Third-party Services

- [VROOM Project](https://github.com/VROOM-Project/vroom) - Vehicle Routing Open-source Optimization Machine
- [Project OSRM](http://project-osrm.org/) - Open Source Routing Machine

These values are configurable in the `~/.sextant.conf` file for a given instance
should you want to run your own VROOM instance.


### Open Data

A core value and approach of EOTL, in addition to open-source *software*, is to make
our tools generate, consume, and publish datasets which participate in *open-data* by complying with
standards and specifications, when and wherever possible.

The only open-data *Sextant* is using is hosted in the `core`
repository of our Codeberg organization and pertains to `zones` which are
polygon boundaries of geodata.

- [data/json/zones](https://codeberg.org/eotl/core/src/branch/main/data/)


### Zones Data

Similar to various OpenStreetMap related apps- upon installing a user can choose
which *regions* or *layers* of mapping data to install. Usually this is scoped
to Nation and County boundaries. Oftentimes, big dense cities are their own datasets.
Given that *Sextant* is currently focused on **urban logistics** our reference implementation is the city of Berlin, it's
Districts, and smaller Neighborhoods within those Districts (called *"Kiezes"* in Berlin). An abstract representation of this looks like this:

```
City B
├── District F
│   ├─ Neighborhood B 
│   ├─ Neighborhood W 
│   └─ Neighborhood R
├── District K
└── District N
    ├─ Neighborhood S
    ├─ Neighborhood W
    ├─ Neighborhood K
    └─ Neighborhood R
```

A delivery rider might deliver across the whole City or two districts. Or
perhaps even just their Neighborhood. In EOTL's tools we use the word `Zone` to
refer to any one of these nested areas.


### Geocoding

The flow of what Sextant does when performing reverse-geocoding lookups (Normal street
`Address 123` -> Lat,Lon geo-coordinates `52.5016672,13.4486899`) is the following flow:

```
      Reverse-Geocode
            |
            |
      Check In Cache? 
        /         \
       /           \
     Yes           No
      |             |
  Use Cache      OSRM Req.
      |             |
      |    <---  Cache It 
      |
 Save Place
   In DB
      |
      |
  Return Result
```

We use the cache to reduce the amount of network requests with respect to the
data model of `Places` and their corresponding `Groups` access control- if two
discreet Groups lookup and want to add the same `Place`, *Sextant* only makes
one outbound request to the OSRM service.

The cache is located at the standard location of the operating system which
*Sextant* is running on. On Debian this is in the users home directory:

```
~/.cache/eotl/location/
```

The cache consists of JSON files which are `sha256` hashes of the input
addresses which was at one time reverse-geocoded. The following cached location
file:

```
10cbe70aedd740397e688361531f6ee6a335b9e78996627b00aa841939cab843
```

Contains the following JSON data:

```
{
  "lon":13.4486899,
  "lat":52.5016672,
  "words":"small pledge plug online rich"
}
```

The `words` value is a locally generated mapping of geo-coordinates to `Where39`
words, read more below if you are interested, but at present that is just an
experimental feature.


### VROOM

*Sextant* without VROOM allows complete multi-stop Tour planning in a simple
user-friendly fashion. Using the VROOM engine, allows for professional level
optimization of Tours and load balancing with multiple inputs such as vehicle
capacity, load size, and starting and ending points.

VROOM integration is not working at the moment, but we are [working on
it](https://codeberg.org/eotl/sextant/issues/41).


### Where39

In an experiment towards usability and internationalization relating to
addresses- we implemented the open-source
[Where39](https://github.com/arcbtc/where39) specification, which is like
geocoding (but with words) and was inspired by `what3words` but uses `BIP39`
words, which is an open-source standardized word-list. This allows for sharing
relatively precise places where there are no street addresses (nor available
open-data) in human friendly words instead of geo-coordinates.

*Note: Where39 can also offer a minor layer of privacy as the words which are
selected use an initialization vector which can differ from instance to
instance of Sextant. However, this is at the expense of open-data sharing of
Places.*
