---
title: Developing
titleFull: Developing Sextant
leafs: ["sextant", "cli", "web-ui", "build-tags"]
lang: "English"
---

## Overview

Sextant is a Go project which also includes a front-end. This means you need to
set up two technology stacks when working on the project. We've tried to make
this as simple as possible to do. And if you only focus on one stack, the other
shouldn't get in your way.

## Quick start

### Install Go

You need to install Go minimum version `1.21.0` to build Sextant. The official Go
installation docs are pretty good: [go.dev/doc/install](https://go.dev/doc/install).
Don't forget to handle the `PATH=...` step or you won't be able to find binaries
that are built/installed by Go.

### Install system requirements

E.g. on Debian:

```
$ sudo apt install -y curl git tar jq make
```

### Building Sextant Sever `sextantd`

The default make target will bootstrap a default Sextant for you.

```
$ make
```

This will create two binaries in the current working directory:

- `sextant`: the command-line tool
- `sextantd`: the HTTP server

You can run the server with `./sextantd -v` to show more info

### Building for development

If you only want to rebuild the
binaries while hacking (and e.g. not the theme/UI files), you can use the
following target:

```
$ make build
```

### The Sextant Web UI

If you're not hacking on the front-end and only concerned with changing the Go backend, then you can skip this step! 

The previous `make` target downloaded all the theme/UI assets required to get moving.

To work on **Web UI** please see the documentation for: [Web UI](web-ui.md)


### Go Build Tags

The project supports the following build tags:

- **bundle**: Bundle web UI files into the binary

These are invoked using the `-tags=<tag-name>` flag for `go build ...`. In
general, build tags allow the Go compiler to conditionally select files for
compilation. This allows us to e.g. build a binary that bundles the theme files
or a binary that reads them from the filesystem.
