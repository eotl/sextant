---
title: VROOM 
titleFull: Various VROOM notes
kind: documenation
leafs: ["sextant", "notes"]
lang: "English"
---

Here is various notes about the [VROOM](https://github.com/VROOM-Project/vroom) logistics engine used in *Sextant*.


## What is the difference between Jobs and Shipments?

From the [VROOM API](https://github.com/VROOM-Project/vroom/blob/master/docs/API.md)

> It is assumed that all delivery-related amounts for jobs
> are loaded at vehicle start, while all pickup-related
> amounts for jobs are brought back at vehicle end.

With Jobs, the solver assumes that a vehicle (rider) has all items to deliver
from its starting position. Capacity / delivery amount constraints seem to be
ignored (**TODO**: to be verified).

A Shipment consists of a pickup and a delivery location. A vehicle starts from
its location with load zero. The capacity restriction of a rider (maximum load)
is respected.
