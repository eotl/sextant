package sextant

type SsrAuthRule struct {
	Path string
	Mode string
}

type SsrAuthRuleset struct {
	Rules []SsrAuthRule
}

func (rs *SsrAuthRuleset) Add(path string, mode string) {
	rs.Rules = append(rs.Rules, SsrAuthRule{path, mode})
}

type SsrAuthService interface {
	WriteRules(key string, rs SsrAuthRuleset) error
	RemoveRules(key string) error
}
