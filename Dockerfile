FROM golang:1.21-alpine AS build

ENV CGO_ENABLED=1

COPY . /sextant
WORKDIR /sextant

RUN apk add --no-cache \
  curl \
  gcc \
  git \
  jq \
  make \
  musl-dev \
  tar

RUN make theme
RUN make ui
RUN make config
RUN make bundle
RUN make build TAGS=bundle

FROM alpine:3.19.1

RUN apk add --no-cache \
  bash \
  git

RUN adduser --system sextant

USER sextant

COPY --from=build /sextant/sextant /usr/bin/sextant
COPY --from=build /sextant/sextantd /usr/bin/sextantd

CMD ["/usr/bin/sextantd", "-v"]
