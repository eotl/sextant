package txt

import (
	"eotl.supply/sextant"
	"io/ioutil"
	"os"
	"testing"
)

func TestWriteRules(t *testing.T) {
	wantFile := "testdata/want/ssrauth.txt"
	// create and open a temporary file
	tmpFile, err := os.CreateTemp("", "sextant-test-")
	if err != nil {
		t.Fatal(err)
	}
	defer tmpFile.Close()
	defer os.Remove(tmpFile.Name())

	ruleSet := sextant.SsrAuthRuleset{}
	ruleSet.Add("/some/path", "rw")
	svc := NewSsrAuthService(tmpFile.Name(), false)
	svc.WriteRules("key123", ruleSet)

	if fileContents, err := readFileContents(tmpFile.Name(), wantFile); err != nil {
		t.Fatal(err)
	} else if got, want := fileContents[0], fileContents[1]; got != want {
		t.Errorf("WriteFiles() -> \ngot:\n%v\n\nwant\n%v", got, want)
	}

	t.Run("Duplicate", func(t *testing.T) {
		svc.WriteRules("key123", ruleSet)
		if fileContents, err := readFileContents(tmpFile.Name(), wantFile); err != nil {
			t.Fatal(err)
		} else if got, want := fileContents[0], fileContents[1]; got != want {
			t.Errorf("WriteFiles() -> \ngot:\n%v\n\nwant\n%v", got, want)
		}
	})
}

func TestRemoveRules(t *testing.T) {
	wantFile := "testdata/want/ssrauth.txt"
	tmpFile, err := os.CreateTemp("", "sextant-test-")
	if err != nil {
		t.Fatal(err)
	}
	defer tmpFile.Close()
	defer os.Remove(tmpFile.Name())

	data := "key123 /some/path rw\nkey999 /some/other/path r"
	if _, err = tmpFile.Write([]byte(data)); err != nil {
		t.Fatal("Failed to write to temporary file", err)
	}
	svc := NewSsrAuthService(tmpFile.Name(), false)
	svc.RemoveRules("key999")

	if fileContents, err := readFileContents(tmpFile.Name(), wantFile); err != nil {
		t.Fatal(err)
	} else if got, want := fileContents[0], fileContents[1]; got != want {
		t.Errorf("WriteFiles() -> \ngot:\n%v\n\nwant\n%v", got, want)
	}
}

func readFileContents(filePaths ...string) (contents []string, err error) {
	for _, fp := range filePaths {
		if c, err := ioutil.ReadFile(fp); err != nil {
			return contents, err
		} else {
			contents = append(contents, string(c))
		}
	}
	return contents, nil
}
