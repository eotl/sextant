package txt

import (
	"bufio"
	"bytes"
	"eotl.supply/sextant"
	"eotl.supply/sextant/helper"
	"fmt"
	"os"
	"strings"
)

var _ sextant.SsrAuthService = (*SsrAuthService)(nil)

type SsrAuthService struct {
	ssrAuthFilepath string
	logger          *sextant.Logger
}

func NewSsrAuthService(ssrAuthFilepath string, doLog bool) *SsrAuthService {
	logger := &sextant.Logger{doLog, os.Stderr}
	return &SsrAuthService{ssrAuthFilepath, logger}
}

func (s *SsrAuthService) WriteRules(key string, rs sextant.SsrAuthRuleset) (err error) {
	// Open or create auth file in append and write-only mode
	ssrAuthFile, err := os.OpenFile(s.ssrAuthFilepath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer ssrAuthFile.Close()
	for _, rule := range rs.Rules {
		line := fmt.Sprintf("%s %s %s", key, rule.Path, rule.Mode)
		if success, err := helper.FileHasLine(ssrAuthFile.Name(), line); err != nil {
			return err
		} else if success {
			s.logger.Printf("%10s: %s\n", "Skipped", line)
			continue
		}
		fmt.Fprintf(ssrAuthFile, "%s\n", line)
		s.logger.Printf("%10s: %s\n", "Added", line)
	}
	return nil
}

func (s *SsrAuthService) RemoveRules(key string) (err error) {
	ssrAuthFile, err := os.Open(s.ssrAuthFilepath)
	if err != nil {
		return err
	}
	defer ssrAuthFile.Close()

	var bs []byte
	buf := bytes.NewBuffer(bs)

	scanner := bufio.NewScanner(ssrAuthFile)
	prefix := fmt.Sprintf("%s ", key)
	for scanner.Scan() {
		if !strings.HasPrefix(scanner.Text(), prefix) {
			if _, err := buf.Write(scanner.Bytes()); err != nil {
				return err
			}
			if _, err = buf.WriteString("\n"); err != nil {
				return err
			}
		} else {
			s.logger.Printf("%10s: %s\n", "Removed", scanner.Text())
		}
	}
	if err := scanner.Err(); err != nil {
		return err
	}

	if err = os.WriteFile(ssrAuthFile.Name(), buf.Bytes(), 0600); err != nil {
		return err
	}
	return nil
}
