package inmem

import (
	"io"
	"net/http"
)

const VroomDefaultUrl = "http://localhost:3000"

type VroomHttpClient struct {
	client *http.Client
	url    string
}

func NewVroomHttpClient(url string) *VroomHttpClient {
	// If url is "" fallback to default
	if len(url) < 1 {
		url = VroomDefaultUrl
	}
	return &VroomHttpClient{http.DefaultClient, url}
}

func (c *VroomHttpClient) Get(url string) (*http.Response, error) {
	return c.client.Get(url)
}

func (c *VroomHttpClient) Post(url, contentType string, body io.Reader) (*http.Response, error) {
	if len(url) < 1 {
		url = c.url
	}
	return c.client.Post(url, contentType, body)
}
