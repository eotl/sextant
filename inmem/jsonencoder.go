package inmem

import (
	"encoding/json"
	"eotl.supply/sextant"
	"errors"
	"fmt"
)

type JsonEnvelope struct {
	Type string
	Msg  interface{}
}

type JsonEncoder struct {
}

func (j *JsonEncoder) EncodeToJson(input interface{}) ([]byte, error) {
	env := JsonEnvelope{
		Type: fmt.Sprintf("%T", input),
		Msg:  input,
	}
	output, err := json.Marshal(env)
	if err != nil {
		return nil, err
	}
	return output, nil
}

func (j *JsonEncoder) DecodeFromJson(input []byte) (interface{}, error) {
	var msg json.RawMessage
	env := JsonEnvelope{
		Msg: &msg,
	}
	if err := json.Unmarshal([]byte(input), &env); err != nil {
		return nil, err
	}
	switch env.Type {

	case "sextant.Contact":
		var p sextant.Contact
		if err := json.Unmarshal(msg, &p); err != nil {
			return nil, err
		}
		return p, nil

	case "[]sextant.Contact":
		var p []sextant.Contact
		if err := json.Unmarshal(msg, &p); err != nil {
			return nil, err
		}
		return p, nil

	case "sextant.Tour":
		var p sextant.Tour
		if err := json.Unmarshal(msg, &p); err != nil {
			return nil, err
		}
		return p, nil

	case "sextant.VroomResponse":
		var p sextant.VroomResponse
		if err := json.Unmarshal(msg, &p); err != nil {
			return nil, err
		}
		return p, nil

	default:
		errMsg := fmt.Sprintf("unknown message type: %q", env.Type)
		return nil, errors.New(errMsg)
	}

}
