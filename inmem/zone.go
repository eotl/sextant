package inmem

import (
	"context"
	"eotl.supply/sextant"
	"regexp"
	"strings"
)

// Ensure type implements interface
var _ sextant.ZoneService = (*ZoneService)(nil)

type ZoneService struct {
}

func NewZoneService() *ZoneService {
	return &ZoneService{}
}

func (s *ZoneService) CreateZone(ctx context.Context, location *sextant.Zone) error {
	return sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func (s *ZoneService) DeleteZone(ctx context.Context, id int) error {
	return sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func (s *ZoneService) FindZoneByAbbr(ctx context.Context, abbr string) (*sextant.Zone, error) {
	return nil, sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func (s *ZoneService) FindZoneByID(ctx context.Context, id int) (*sextant.Zone, error) {
	return nil, sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func (s *ZoneService) FindZones(ctx context.Context, filter sextant.ZoneFilter) ([]*sextant.Zone, int,
	error) {
	return nil, 0, sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func (s *ZoneService) SetZoneAbbr(zones []sextant.Zone) ([]sextant.Zone, error) {
	for i, _ := range zones {
		str := removeVowels(zones[i].Name)
		zones[i].Abbr = strings.ToUpper(str)
	}
	return zones, nil
}

func removeVowels(word string) string {
	re := regexp.MustCompile(`[aAeEiIoOuUäÄöÖüÜ]`)
	return re.ReplaceAllString(word, "")
}
