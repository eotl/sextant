package inmem

import (
	"eotl.supply/sextant"
	"time"
)

var _ sextant.DateTimeService = (*DateTimeService)(nil)

type DateTimeService struct {
}

func NewDateTimeService() *DateTimeService {
	return &DateTimeService{}
}

func (service *DateTimeService) Create(s string) (*sextant.DateTime, error) {
	ttime, err := time.Parse(sextant.TimeLayout, s)
	if err != nil {
		return nil, err
	}
	return &sextant.DateTime{ttime}, nil
}
