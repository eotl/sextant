package inmem

import (
	"eotl.supply/sextant"
	w39 "github.com/fusion44/go_where39"
	"strings"
)

type LocationWordService struct {
	ShuffleValue int
}

func NewLocationWordService() *LocationWordService {
	return &LocationWordService{52}
}

func (service *LocationWordService) ToWords(l *sextant.Location) (string, error) {
	coords := w39.LatLng{Lat: l.Lat, Lng: l.Lon}
	sA, err := w39.ToWords(coords, service.ShuffleValue)
	if err != nil {
		return "", err
	}
	return strings.Join(sA, " "), nil
}

func (service *LocationWordService) FromWords(s string) (*sextant.Location, error) {
	sA := strings.Split(s, " ")
	latlng, err := w39.FromWords(sA, service.ShuffleValue)
	if err != nil {
		return nil, err
	}
	return &sextant.Location{Lat: latlng.Lat, Lon: latlng.Lng}, nil
}
