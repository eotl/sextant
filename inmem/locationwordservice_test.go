package inmem

import (
	"eotl.supply/sextant"
	"fmt"
	"strconv"
	"testing"
)

func TestLocationWordService(t *testing.T) {
	s := NewLocationWordService()
	location := &sextant.Location{Lon: 13.469098764980004, Lat: 52.48324999949}
	t.Run("ToWords", func(t *testing.T) {
		tcs := []struct {
			input   *sextant.Location
			want    string
			wantErr string
		}{
			{location, "small claw image want awake", ""},
		}

		for _, tc := range tcs {
			got, gotErr := s.ToWords(tc.input)
			if gotErr != nil {
				if gotErr.Error() != tc.wantErr {
					t.Errorf("ToWords() -> got: %v want: %v", gotErr.Error(), tc.wantErr)
				}
			}
			if got != tc.want {
				t.Errorf("ToWords() -> got: %v want: %v", got, tc.want)
			}
		}
	})

	t.Run("FromWords", func(t *testing.T) {
		tcs := []struct {
			input   string
			want    *sextant.Location
			wantErr string
		}{
			{"small claw image want awake", location, ""},
		}

		for _, tc := range tcs {
			got, gotErr := s.FromWords(tc.input)
			if gotErr != nil {
				if gotErr.Error() != tc.wantErr {
					t.Errorf("FromWords() -> gotErr: %v wantErr: %v input: '%s'", gotErr.Error(), tc.wantErr, tc.input)
				}
			}
			if !locationEqual(got, tc.want, 5) {
				t.Errorf("FromWords() -> \ngot:  %v\nwant: %v\ninput: '%s'", got, tc.want, tc.input)
			}
		}
	})
}

func locationEqual(l1 *sextant.Location, l2 *sextant.Location, precision int) bool {
	f := fmt.Sprintf("%%.%sf", strconv.Itoa(precision))
	if fmt.Sprintf(f, l1.Lat) != fmt.Sprintf(f, l2.Lat) {
		return false
	}
	if fmt.Sprintf(f, l1.Lon) != fmt.Sprintf(f, l2.Lon) {
		return false
	}
	return true
}
