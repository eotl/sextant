package inmem

import (
	"eotl.supply/sextant"
	"fmt"
)

var _ sextant.GeojsonService = (*GeojsonService)(nil)

type GeojsonService struct {
}

func NewGeojsonService() *GeojsonService {
	return &GeojsonService{}
}

func (s *GeojsonService) CreateFeature(value interface{}) *sextant.GeojsonFeature {
	f := sextant.GeojsonFeature{Type: "Feature"}

	switch t := value.(type) {

	case sextant.ContactAddress:
		contact := value.(sextant.ContactAddress)
		f.Properties = map[string]string{
			"name":            contact.Name,
			"email":           contact.Email,
			"address":         contact.Label,
			"address_details": contact.Details,
			"phone":           contact.Phone,
			"location_words":  contact.Location.Words,
		}
		f.Geometry = sextant.GeojsonGeometry{
			Type:        "Point",
			Coordinates: contact.Location.ToArray(),
		}

	case sextant.VroomRoute:
		route := value.(sextant.VroomRoute)
		coordinates := []*[2]float64{}
		for _, rs := range route.Steps {
			coordinates = append(coordinates, rs.Location)
		}
		f.Geometry = sextant.GeojsonGeometry{
			Type:        "LineString",
			Coordinates: coordinates,
		}
		f.Properties = map[string]string{}

	default:
		fmt.Printf("Unkown type: %T", t)

	}

	return &f
}

func (s *GeojsonService) CreateFeatureCollection(value interface{}) *sextant.GeojsonFeatureCollection {
	c := sextant.GeojsonFeatureCollection{
		Type:     "FeatureCollection",
		Features: []*sextant.GeojsonFeature{},
	}

	switch t := value.(type) {

	case []sextant.ContactAddress:
		contacts := value.([]sextant.ContactAddress)
		for _, contact := range contacts {
			if contact.Location == nil {
				continue
			}
			feature := s.CreateFeature(contact)
			c.Features = append(c.Features, feature)
		}

	case []sextant.VroomRoute:
		routes := value.([]sextant.VroomRoute)
		for _, route := range routes {
			feature := s.CreateFeature(route)
			c.Features = append(c.Features, feature)
		}

	default:
		fmt.Printf("Unkown type: %T", t)
	}

	return &c
}
