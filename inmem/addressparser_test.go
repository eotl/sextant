package inmem

import (
	"testing"
)

func TestAddressParserParse(t *testing.T) {
	parser := NewAddressParser()

	testdata := map[string]string{
		"Pfalzburger Strasse 71, 10719 Berlin":                               "Pfalzburger Strasse 71, 10719 Berlin",
		"c/o Mayer, Rochowstrasse 8, Berlin 10245":                           "Rochowstrasse 8, Berlin 10245",
		"wallstraße 5, apt. 2.5.2, 10179 Berlin":                             "wallstraße 5, 10179 Berlin",
		"Novalisstrasse 11 (Vorderhaus), 10115 Berlin-Mitte":                 "Novalisstrasse 11, 10115 Berlin-Mitte",
		"Lychener Str, 27, 10437Berlin":                                      "Lychener Str, 27, 10437 Berlin",
		"Kaiserdamm 25a, 2Etage, 14057, Berlin":                              "Kaiserdamm 25a, 14057, Berlin",
		"Eisenacher Strasse 41, 5th floor, 10781, Berlin.":                   "Eisenacher Strasse 41, 10781, Berlin.",
		"Lichtenberger Str. 41, Apt. 404, 10243 Berlin":                      "Lichtenberger Str. 41, 10243 Berlin",
		"C/O Klaus, Birkenweg 15, 12000, Berlin":                             "Birkenweg 15, 12000, Berlin",
		"Apartment WE 4, first floor left\nDefreggerstraße 21, 12435 Berlin": "Defreggerstraße 21, 12435 Berlin",
	}

	for given, want := range testdata {
		got := parser.ParseAddress(given)
		t.Run("", func(t *testing.T) {
			if want != got {
				t.Errorf("ParseAddress() -> given\t%s\n\ngot\t%s\nwant\t%s", given, got, want)
			}
		})
	}
}
