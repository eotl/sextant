package inmem

import (
	"eotl.supply/sextant"
	"regexp"
	"strings"
)

var _ sextant.AddressParser = (*AddressParser)(nil)

type AddressParser struct {
}

func NewAddressParser() *AddressParser {
	return &AddressParser{}
}

func (parser *AddressParser) ParseAddress(address string) string {
	var keep []string
	temp := strings.Split(address, ",")
	// Match "( any kind of content in parantheses  )" so we can remove it
	re_invalid1 := regexp.MustCompile(` \(.*\)`)
	// Match "2Etage"
	re_invalid2 := regexp.MustCompile(` \d{1,4}[A-Za-z]{4,}`)
	// Match e.g. "apt. 2.5.2"
	re_invalid3 := regexp.MustCompile(`\.\d`)
	// Match e.g. "C/O Johnson"
	re_invalid4 := regexp.MustCompile(`[cC]/[oO] [A-Za-z]{2,}`)
	// Match e.g "7th floor"
	re_invalid5 := regexp.MustCompile(`\d{1,2}th [A-Za-z]{2,}`)
	// Match e.g "Apt. 404"
	re_invalid6 := regexp.MustCompile(`[Aa]p.*\d+`)
	// Match e.g "first floor left\n""
	re_invalid7 := regexp.MustCompile(`[^\d].*\n`)

	// Match "12345City" so we can fix it: "12345 City"
	re_group := regexp.MustCompile(`(?P<postalcode>\d{5})(?P<city>[A-Za-z]{2,})`)

	for i, item := range temp {
		item = re_invalid1.ReplaceAllString(item, "")
		item = re_invalid2.ReplaceAllString(item, "")
		item = re_invalid4.ReplaceAllString(item, "")
		item = re_invalid5.ReplaceAllString(item, "")
		item = re_invalid6.ReplaceAllString(item, "")
		item = re_invalid7.ReplaceAllString(item, "")
		if len(item) > 0 {
			match_map := parser.findNamedMatches(re_group, item)
			if len(match_map) > 0 {
				item = match_map["postalcode"] + " " + match_map["city"]
			}
			item = strings.TrimSpace(item)
		}
		temp[i] = item
	}
	for _, item := range temp {
		if len(item) == 0 ||
			re_invalid3.MatchString(item) {
			continue
		}
		keep = append(keep, item)
	}
	out := strings.Join(keep, ", ")
	return out
}

func (parser *AddressParser) findNamedMatches(regex *regexp.Regexp, str string) map[string]string {
	match := regex.FindStringSubmatch(str)

	results := map[string]string{}
	for i, name := range match {
		results[regex.SubexpNames()[i]] = name
	}
	return results
}
