package inmem

import (
	"bytes"
	"encoding/json"
	"eotl.supply/sextant"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

var _ sextant.VroomService = (*VroomService)(nil)

type VroomService struct {
	client sextant.HttpClient
	logger *sextant.Logger
}

// Optionally pass client parameter, e.g. when mocking
func NewVroomService(client sextant.HttpClient, url string, doLog bool) *VroomService {
	if client == nil {
		client = NewVroomHttpClient(url)
	}
	logger := &sextant.Logger{doLog, os.Stderr}
	return &VroomService{client, logger}
}

func (service *VroomService) Query(request *sextant.VroomRequest) (*sextant.VroomResponse, error) {
	data, err := json.Marshal(request)
	if err != nil {
		err = fmt.Errorf("error marshaling vroom request to json: %v", err)
		return nil, err
	}
	// Post with url parameter "" to fallback client.Url
	httpResp, err := service.client.Post("", "application/json", bytes.NewBuffer(data))
	if err != nil {
		err = fmt.Errorf("http post: %s failed with %v", "", err)
		return nil, err
	}
	defer httpResp.Body.Close()
	var response sextant.VroomResponse
	if httpResp.StatusCode != http.StatusOK {
		if err := json.NewDecoder(httpResp.Body).Decode(&response); err != nil {
			return nil, fmt.Errorf("http post: %s with status code %v", "", httpResp.StatusCode)
		}
		return &response, nil
	}
	if err := json.NewDecoder(httpResp.Body).Decode(&response); err != nil {
		return nil, fmt.Errorf("parsing json response from %s: %v", "", err)
	}
	return &response, nil
}

func (service *VroomService) QueryWithFile(f string) (*sextant.VroomResponse, error) {
	service.logger.Printf("Reading JSON file '%s' ... ", f)
	data, err := ioutil.ReadFile(f)
	if err != nil {
		err = fmt.Errorf("unable to read json file %s, %v\n", f, err)
		fmt.Println("")
		return nil, err
	}
	var request sextant.VroomRequest
	err = json.Unmarshal(data, &request)
	if err != nil {
		err = fmt.Errorf("unable to unmarshal json data %s, %v\n", data, err)
		return nil, err
	}
	service.logger.Printf("%s\n", "done.")
	return service.Query(&request)
}

// QUESTION: How to handle 'contact.Location == nil' cases?
func (service *VroomService) CreateRequest(tour sextant.Tour) *sextant.VroomRequest {
	request := sextant.VroomRequest{}
	request.Jobs = []*sextant.VroomJob{}
	request.Shipments = []*sextant.VroomShipment{}

	if len(tour.Pickups) > 0 {
		// ENHANCE: support multiple pickup locations
		pickup := tour.Pickups[0]
		if pickup.Location == nil {
			service.logger.Printf("Pickup has no location: %v\n", pickup)
		} else {
			i := 0
			for _, delivery := range tour.Deliveries {
				if delivery.Location == nil {
					service.logger.Printf("Delivery has no location: %v\n", delivery)
					continue
				}
				shipment_ptr := service.createShipment(*delivery.ContactAddress, *pickup.ContactAddress, i)
				request.Shipments = append(request.Shipments, shipment_ptr)
				i = i + 2
			}
		}
	} else {
		i := 0
		for _, delivery := range tour.Deliveries {
			if delivery.Location == nil {
				continue
			}
			i = i + 1
			job := sextant.VroomJob{
				ID:          i,
				Location:    delivery.Location.ToArray(),
				Description: fmt.Sprintf("%d %s", i, delivery.Name),
				Delivery:    []int{delivery.Amount},
				TimeWindows: delivery.TimeWindows(),
			}
			request.Jobs = append(request.Jobs, &job)
		}
	}

	i := 0
	request.Vehicles = []*sextant.VroomVehicle{}
	for _, rider := range tour.Riders {
		if rider.Address == nil || rider.Location == nil {
			continue
		}
		i = i + 1
		vehicle := sextant.VroomVehicle{
			ID:          i,
			Description: rider.Name,
			Profile:     "bicycle",
			Capacity:    []int{rider.Amount},
			Start:       rider.Location.ToArray(),
			End:         rider.Location.ToArray(),
			TimeWindow:  rider.TimeWindow(),
		}
		request.Vehicles = append(request.Vehicles, &vehicle)
	}
	request.Options = &sextant.VroomOptions{G: true}
	return &request
}

func (service *VroomService) createShipment(delivery sextant.ContactAddress, pickup sextant.ContactAddress, index int) *sextant.VroomShipment {
	index++
	shipment := sextant.VroomShipment{}
	shipment.Pickup = sextant.VroomShipmentStep{
		ID:          index,
		Location:    pickup.Location.ToArray(),
		Description: pickup.Name,
		TimeWindows: pickup.TimeWindows(),
	}
	shipment.Delivery = sextant.VroomShipmentStep{
		ID:          index,
		Location:    delivery.Location.ToArray(),
		Description: delivery.Name,
		TimeWindows: delivery.TimeWindows(),
	}
	shipment.Amount = []int{delivery.Amount}
	return &shipment
}
