package inmem

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"reflect"
	"testing"
)

type Contact struct {
	Name string
}

func TestGetFileNameWithoutSuffix(t *testing.T) {
	service := NewFileService(false)
	f := "/home/user/.data/myfile.txt"
	expected := "myfile"
	actual := service.GetFileNameWithoutSuffix(f)
	if actual != expected {
		t.Errorf("GetFileNameWithoutSuffix(%s) -> expected %s, actual: %s", f, expected, actual)
	}
}

func TestWriteToJSON(t *testing.T) {
	service := NewFileService(false)
	var joe = Contact{"joe"}
	tmpDir, err := ioutil.TempDir("", "vroom")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(tmpDir)

	t.Run("successfully writes file", func(t *testing.T) {
		want := true
		got, err := service.ToJSON("test.json", joe, tmpDir)
		if err != nil {
			log.Fatal(err)
		}
		if got != want {
			t.Errorf("WriteToJSON() -> want %v got %v", want, got)
		}

		t.Run("file content is correct", func(t *testing.T) {
			srcFile := service.makePath(tmpDir, "test.json")
			jsonFile, err := os.Open(srcFile)
			if err != nil {
				log.Fatal(err)
			}
			defer jsonFile.Close()
			data, err := ioutil.ReadAll(jsonFile)
			if err != nil {
				log.Fatal(err)
			}
			var contact_got Contact
			err = json.Unmarshal(data, &contact_got)
			if err != nil {
				log.Fatal(err)
			}
			if !reflect.DeepEqual(joe, contact_got) {
				t.Errorf("WriteToJSON() -> want %v got %v", joe, contact_got)
			}
		})
	})

	t.Run("write failure gives error", func(t *testing.T) {
		_, err := service.ToJSON("", joe, "")
		if err == nil {
			t.Errorf("WriteToJSON() -> write failure should give an error")
		}
		want := "can not open file  for writing: open : no such file or directory"
		got := err.Error()
		if want != got {
			t.Errorf("WriteToJSON() -> want: %v got: %v\n", want, got)
		}
	})
}

func TestSetFirstLine(t *testing.T) {
	service := NewFileService(false)
	tcs := []struct {
		name        string
		inputFile   string
		inputHeader string
		inputAction string
		wantFile    string
		wantErr     string
	}{
		{"Prepend", "testdata/input/deliveries.csv", "Name, Email", "prepend", "testdata/want/deliveries.prepend.csv", ""},
		{"Replace", "testdata/input/deliveries.csv", "Name, Email", "replace", "testdata/want/deliveries.replace.csv", ""},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			got, gotErr := service.SetFirstLine(tc.inputFile, tc.inputHeader, tc.inputAction)
			if gotErr != nil {
				if gotErr.Error() != tc.wantErr {
					t.Errorf("SetCsvHeader -> got err\n%+v\nwant err\n%+v", gotErr, tc.wantErr)
				}
			}
			defer os.Remove(got)
			cmd := exec.Command("git", "diff", got, tc.wantFile)
			out, err := cmd.CombinedOutput()
			if err != nil {
				t.Errorf("SetCsvHeader -> got %+v want %+v\ndiff\n %+v", got, tc.wantFile, string(out))
			}
		})
	}
}
