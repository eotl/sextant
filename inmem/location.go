package inmem

import (
	"context"
	"encoding/json"
	"eotl.supply/sextant"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"
)

// Ensure type implements interface
var _ sextant.LocationService = (*LocationService)(nil)

type LocationService struct {
	Client      sextant.HttpClient
	Cache       sextant.LocationCache
	wordService sextant.LocationWordService
	logger      *sextant.Logger
}

func NewLocationService(client sextant.HttpClient, url string, cache sextant.LocationCache, doLog bool) *LocationService {
	if client == nil {
		client = NewNominatimHttpClient(url)
	}
	if cache == nil {
		cache = NewLocationCache("", []string{"eotl", "location"})
	}
	logger := &sextant.Logger{doLog, os.Stderr}
	wordService := NewLocationWordService()
	return &LocationService{client, cache, wordService, logger}
}

func (s *LocationService) FindOrCreateLocation(ctx context.Context, location *sextant.Location) error {
	return sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func (s *LocationService) FindLocationByID(ctx context.Context, id int) (*sextant.Location, error) {
	return nil, sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func (s *LocationService) FetchLocation(address string) (*sextant.Location, error) {
	return s.getLocationCached(address)
}

func (s *LocationService) getLocationCached(address string) (*sextant.Location, error) {
	key := s.Cache.GetKey(address)
	location_ptr, err := s.Cache.Load(key)

	if err != nil {
		err := fmt.Errorf("error loading from cache %v : %v", address, err)
		return nil, err
	}
	if location_ptr != nil {
		s.logger.Printf("%s", "c")
	}
	if location_ptr == nil {
		// fetch location directly
		location_ptr, err = s.getLocation(address)
		if err != nil {
			err = fmt.Errorf("error fetching location from server '%s': %v", address, err)
			//s.logger.Printf("%v", err)
			s.logger.Printf("%s", "x")
			return nil, err
		}
		err = s.Cache.Store(key, location_ptr)
		if err != nil {
			err := fmt.Errorf("error storing cache entry %v : %v", address, err)
			return nil, err
		}
		s.logger.Printf("%s", ".")
		time.Sleep(1 * time.Second)
	}
	return location_ptr, nil
}

func (s *LocationService) getLocation(address string) (*sextant.Location, error) {
	params := url.Values{
		"q":      {address},
		"limit":  {"1"},
		"format": {"json"},
	}
	path := "?" + params.Encode()
	resp, err := s.Client.Get(path)
	if err != nil {
		err := fmt.Errorf("http get: %s failed with %v", path, err)
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http get: %s with status code %s", path, resp.Status)
	}
	var locations []sextant.LocationDecode
	if err := json.NewDecoder(resp.Body).Decode(&locations); err != nil {
		return nil, fmt.Errorf("parsing json response from %s with %s", path, err)
	}
	if len(locations) < 1 {
		return nil, fmt.Errorf("%s returned no locations", path)
	}
	location := sextant.Location(locations[0])
	words, err := s.wordService.ToWords(&location)
	if err != nil {
		return nil, fmt.Errorf("could not convert to words: %v -> %v", location, err)
	}
	location.Words = words
	return &location, nil
}

func (s *LocationService) UpdateLocations(people ...[]sextant.ContactAddress) (failed []sextant.ContactAddress) {
	var err error
	failed = []sextant.ContactAddress{}
	s.logger.Printf("%s", "Fetching location data ... [")
	for _, contacts := range people {
		for _, contact := range contacts {
			contact.Location, err = s.getLocationCached(contact.Label)
			if err != nil {
				failed = append(failed, contact)
				continue
			}
		}
	}
	s.logger.Printf("%s\n", "] done.")
	return failed
}
