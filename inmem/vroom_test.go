package inmem

import (
	"encoding/json"
	"reflect"
	"regexp"
	"testing"

	"eotl.supply/sextant"
	"eotl.supply/sextant/mock"
)

// This helper method Creates a Contact with mostly default values
func createContactAddress(name string) sextant.ContactAddress {
	return sextant.ContactAddress{
		Contact: &sextant.Contact{Name: name},
		Address: &sextant.Address{
			Location: &sextant.Location{Lon: 1, Lat: 1, Words: ""},
		},
	}
}

func TestVroomService(t *testing.T) {
	t.Run("CreateRequest", func(t *testing.T) {
		joe := createContactAddress("Joe")
		sam := createContactAddress("Sam")
		store := createContactAddress("Store")

		// Let's mock to ensure no external requests
		client, server := mock.NewHttpClient(200, "")
		defer server.Close()

		vroomService := NewVroomService(client, "", false)

		tcs := []struct {
			name string
			tour sextant.Tour
			want string
		}{
			{
				"DeliveriesOnly",
				sextant.Tour{
					Deliveries: []sextant.TourItem{sextant.TourItem{ContactAddress: &joe}},
					Pickups:    []sextant.TourItem{},
					Riders:     []sextant.TourItem{},
				},
				`{"vehicles":[],"jobs":[{"id":1,"location":[1,1],"description":"1 Joe","delivery":[0]}],"shipments":[],"options":{"g":true}}`,
			},
			{
				"DelivieriesRiders",
				sextant.Tour{
					Deliveries: []sextant.TourItem{sextant.TourItem{ContactAddress: &joe}},
					Pickups:    []sextant.TourItem{},
					Riders:     []sextant.TourItem{sextant.TourItem{ContactAddress: &sam}},
				},
				`{"vehicles":[{"id":1,"description":"Sam","profile":"bicycle","capacity":[0],"start":[1,1],"end":[1,1]}],"jobs":[{"id":1,"location":[1,1],"description":"1 Joe","delivery":[0]}],"shipments":[],"options":{"g":true}}`,
			},
			{
				"DelievriesPickupsRiders",
				sextant.Tour{
					Deliveries: []sextant.TourItem{sextant.TourItem{ContactAddress: &joe}},
					Pickups:    []sextant.TourItem{sextant.TourItem{ContactAddress: &store}},
					Riders:     []sextant.TourItem{sextant.TourItem{ContactAddress: &sam}},
				},
				`{"vehicles":[{"id":1,"description":"Sam","profile":"bicycle","capacity":[0],"start":[1,1],"end":[1,1]}],"jobs":[],"shipments":[{"pickup":{"id":1,"location":[1,1],"description":"Store"},"delivery":{"id":1,"location":[1,1],"description":"Joe"},"amount":[0]}],"options":{"g":true}}`,
			},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				request := vroomService.CreateRequest(tc.tour)
				data, err := json.Marshal(request)
				if err != nil {
					t.Errorf("failed to marshall VroomRequest: %v", err)
				}
				got := string(data)
				if got != tc.want {
					t.Errorf("json.Marshal(VroomRequest) ->\ngot:\t%+v\nwant:\t%+v\n", got, tc.want)
				}
			})
		}
	})

	t.Run("Query", func(t *testing.T) {
		json_valid_response := `{"code":0}`
		json_valid_response_invalid_input := `{"code":0,"error":"Invalid vehicles."}`
		want_valid_response_invalid_input := &sextant.VroomResponse{Code: 0, Error: "Invalid vehicles."}
		json_invalid_response := `{"code"}`
		want_valid_response := &sextant.VroomResponse{}

		tcs := []struct {
			name             string
			input            *sextant.VroomRequest
			mockStatusCode   int
			mockResponseData string
			want_err         string
			want             *sextant.VroomResponse
		}{
			{"Simple", &sextant.VroomRequest{}, 200, json_valid_response, "", want_valid_response},
			{"InvalidInput", &sextant.VroomRequest{}, 400, json_valid_response_invalid_input, "", want_valid_response_invalid_input},
			{"JsonResponseInvalid", &sextant.VroomRequest{}, 200, json_invalid_response, "parsing json response.*invalid character.*", nil},
			{"StatusCodeNot200", &sextant.VroomRequest{}, 400, json_invalid_response, "http post:.*with status code 400", nil},
			{"ServerDown", &sextant.VroomRequest{}, 200, "", "http post:.*connection refused", nil},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				client, server := mock.NewHttpClient(tc.mockStatusCode, tc.mockResponseData)
				defer server.Close()
				service := NewVroomService(client, "", false)
				if tc.name == "ServerDown" {
					server.Close()
				}
				got, got_err := service.Query(tc.input)
				if got_err != nil {
					if ok, _ := regexp.MatchString(tc.want_err, got_err.Error()); !ok {
						t.Errorf("Query() returned unexpected error: %v", got_err)
					}
				}
				if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("Query() -> got %+v, want %+v", got, tc.want)
				}
			})
		}
	})

	t.Run("QueryFromFile", func(t *testing.T) {
		json_valid_response := `{"code":0}`
		want_valid_response := &sextant.VroomResponse{}

		tcs := []struct {
			name             string
			input            string
			mockResponseData string
			want_err         string
			want             *sextant.VroomResponse
		}{
			{"ValidFile", "testdata/input/vroom/valid_vroom_request.json", json_valid_response, "", want_valid_response},
			{"InvalidFile", "testdata/input/vroom/invalid_vroom_request.json", json_valid_response, "unable to unmarshal json data", nil},
			{"MissingFile", "testdata/input/vroom/missing.json", json_valid_response, "no such file or directory", nil},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				client, server := mock.NewHttpClient(200, tc.mockResponseData)
				defer server.Close()
				service := NewVroomService(client, "", false)
				got, got_err := service.QueryWithFile(tc.input)
				if got_err != nil {
					if ok, _ := regexp.MatchString(tc.want_err, got_err.Error()); !ok {
						t.Errorf("QueryWithFile() returned unexpected error: %v", got_err)
					}
				}
				if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("QueryWithFile() -> input %s, got %+v, want %+v", tc.input, got, tc.want)
				}
			})
		}
	})
}

/*
// TODO: are these tests really necessary?

func TestVroomRequest(t *testing.T) {

	t.Run("json.Unmarshal()", func(t *testing.T) {
		tcs := []struct {
			name            string
			input           []byte
			want_deliveries []sextant.Contact
			want_pickups    []sextant.Contact
			want_riders     []sextant.Contact
		}{
			{"ShipmentsOnly",
				[]byte(`{"vehicles":[],"jobs":[],"shipments":[{"pickup":{"id":1,"location":[13.42512, 52.48690],"description":"SuperStore","time_windows":[[1638608400,1638619200]]},"delivery":{"id":1,"location":[13.44575, 52.47865],"description":"Joe","time_windows":[[1638612000,1638619200]]},"amount":[1]}],"options":{"g":true}}`),
				[]sextant.Contact{joe},
				[]sextant.Contact{store},
				[]sextant.Contact{},
			},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				want := *vroomService.CreateRequest(tc.want_deliveries, tc.want_pickups, tc.want_riders)
				var got sextant.VroomRequest
				json.Unmarshal(tc.input, &got)
				if !reflect.DeepEqual(got, want) {
					t.Errorf("json.Unmarshall(%s) ->\ngot:\t%+v\nwant:\t%+v", tc.input, got, want)
				}
			})
		}
	})

}

func TestShipmentStepTimeWindowsToJson(t *testing.T) {
	ts, err := time.Parse("02-01-06 15:00", "12-12-21 15:00")
	if err != nil {
		t.Error(`Unable to parse time`)
	}
	tw := TimeWindow{ts, ts}
	tws := []*TimeWindow{&tw}
	tables := []struct {
		input    ShipmentStep
		expected string
	}{
		{ShipmentStep{}, "{\"id\":0}"},
		{ShipmentStep{TimeWindows: &tws}, "{\"id\":0,\"time_windows\":[[1639321200,1639321200]]}"},
	}
	for _, table := range tables {
		data, err := json.Marshal(table.input)
		if err != nil {
			t.Error(`Unable to jsonMarshal()`)
		}
		actual := string(data)
		if actual != table.expected {
			t.Errorf("json.Marshal(ShipmentStep): input %+v, expected %+v, actual %+v", table.input, table.expected, actual)
		}
	}
}

func TestVehicleTimeWindowToJson(t *testing.T) {
	ts, err := time.Parse("02-01-06 15:00", "12-12-21 15:00")
	if err != nil {
		t.Error(`Unable to parse time`)
	}
	tw := TimeWindow{ts, ts}
	tables := []struct {
		input    Vehicle
		expected string
	}{
		{Vehicle{}, "{\"id\":0}"},
		{Vehicle{TimeWindow: &tw}, "{\"id\":0,\"time_window\":[1639321200,1639321200]}"},
	}
	for _, table := range tables {
		data, err := json.Marshal(table.input)
		if err != nil {
			t.Error(`Unable to jsonMarshal()`)
		}
		actual := string(data)
		if actual != table.expected {
			t.Errorf("json.Marshal(ShipmentStep): input %+v, expected %+v, actual %+v", table.input, table.expected, actual)
		}
	}
}
*/
