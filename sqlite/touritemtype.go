package sqlite

import (
	"context"
	_ "database/sql"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.TourItemtypeService = (*TourItemtypeService)(nil)

type TourItemtypeService struct {
	db *DB
}

func NewTourItemtypeService(db *DB) *TourItemtypeService {
	return &TourItemtypeService{db: db}
}

func (s *TourItemtypeService) FindTourItemtypeByName(ctx context.Context, name string) (*sextant.TourItemtype, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	tourItemtype, err := findTourItemtypeByName(ctx, tx, name)

	if err != nil {
		return nil, err
	} else if err := attachTourItemtypeAssociations(ctx, tx, tourItemtype); err != nil {
		return nil, err
	}

	return tourItemtype, nil
}

func findTourItemtypeByName(ctx context.Context, tx *Tx, name string) (*sextant.TourItemtype, error) {
	tourItemtypes, _, err := findTourItemtypes(ctx, tx, sextant.TourItemtypeFilter{Name: &name})
	if err != nil {
		return nil, err
	} else if len(tourItemtypes) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "TourItemtype not found."}
	}
	return &tourItemtypes[0], nil
}

func createTourItemtype(ctx context.Context, tx *Tx, tit *sextant.TourItemtype) error {
	tit.CreatedAt = tx.now
	tit.UpdatedAt = tit.CreatedAt

	if err := tit.Validate(); err != nil {
		return err
	}

	result, err := tx.ExecContext(ctx, `
		INSERT INTO tour_itemtypes (
			name,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?)
	`,
		tit.Name,
		(*NullTime)(&tit.CreatedAt),
		(*NullTime)(&tit.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	tit.ID = int(id)

	return nil
}

func findTourItemtypes(ctx context.Context, tx *Tx, filter sextant.TourItemtypeFilter) (_ []sextant.TourItemtype, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.Name; v != nil {
		where, args = append(where, "name = ?"), append(args, *v)
	}
	rows, err := tx.QueryContext(ctx, `
		SELECT 
		    id,
				name,
				created_at,
		    updated_at,
		    COUNT(*) OVER()
		FROM tour_itemtypes
		WHERE `+strings.Join(where, " AND ")+`
		ORDER BY id ASC
		`+FormatLimitOffset(filter.Limit, filter.Offset),
		args...,
	)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	items := make([]sextant.TourItemtype, 0)
	for rows.Next() {
		var item sextant.TourItemtype
		if err := rows.Scan(
			&item.ID,
			&item.Name,
			(*NullTime)(&item.CreatedAt),
			(*NullTime)(&item.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		items = append(items, item)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return items, n, nil
}

func attachTourItemtypeAssociations(ctx context.Context, tx *Tx, tour *sextant.TourItemtype) (err error) {
	// Implement me when needed
	return nil
}
