package sqlite_test

import (
	"context"
	_ "fmt"
	"reflect"
	_ "strings"
	"testing"
	_ "time"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
	"github.com/paulmach/orb"
)

func TestLocationService(t *testing.T) {
	t.Run("FindOrCreateLocation", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)

		ctx := context.Background()

		s := sqlite.NewLocationService(db)
		location := &sextant.Location{Lon: 13.412380949017187, Lat: 52.52150585, Words: "yes no maybe"}

		if err := s.FindOrCreateLocation(ctx, location); err != nil {
			t.Fatal(err)
		} else if got, want := location.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, location)
		} else if location.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if location.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		if other, err := s.FindLocationByID(ctx, 1); err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(location, other) {
			t.Fatalf("mismatch: %#v != %#v", location, other)
		}

		t.Run("WithZone", func(t *testing.T) {
			zone := &sextant.Zone{
				Name:     "TestZone",
				Geometry: orb.Polygon{{{0, 0}, {0, 2}, {2, 2}, {2, 0}, {0, 0}}},
			}
			MustCreateZone(t, ctx, db, zone)
			location := &sextant.Location{Lon: 1, Lat: 1}
			if err := s.FindOrCreateLocation(ctx, location); err != nil {
				t.Fatal(err)
			}
			s2 := sqlite.NewZoneMembershipService(db)
			if m, err := s2.FindZoneMembershipByIDs(ctx, zone.ID, location.ID); err != nil {
				t.Fatal(err)
			} else if m == nil {
				t.Fatal("ZoneMembership was no found")
			}
		})
	})
}

func MustCreateLocation(tb testing.TB, ctx context.Context, db *sqlite.DB, location *sextant.Location) *sextant.Location {
	tb.Helper()
	if err := sqlite.NewLocationService(db).FindOrCreateLocation(ctx, location); err != nil {
		tb.Fatal(err)
	}
	return location
}
