package sqlite_test

import (
	"context"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
	"github.com/paulmach/orb"
)

func TestContactAddressService(t *testing.T) {
	db := MustOpenDB(t)
	defer MustCloseDB(t, db)
	ctx := context.Background()

	contactAddress := newContactAddress("Harry", "Times Square 1", "Apartment 404", 1, 1)

	MustCreateAddress(t, ctx, db, contactAddress.Address)
	MustCreateContact(t, ctx, db, contactAddress.Contact)

	s := sqlite.NewContactAddressService(db)

	t.Run("FindOrCreateContactAddress", func(t *testing.T) {
		if err := s.FindOrCreateContactAddress(ctx, contactAddress); err != nil {
			t.Fatal(err)
		} else if got, want := contactAddress.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, contactAddress)
		} else if contactAddress.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if contactAddress.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		t.Run("WithDuplicate", func(t *testing.T) {
			contactAddressDup := newContactAddress("Harry", "Times Square 1", "Apartment 404", 1, 1)

			if err := s.FindOrCreateContactAddress(ctx, contactAddressDup); err != nil {
				t.Fatal(err)
			} else if got, want := contactAddressDup.ID, contactAddress.ID; got != want {
				t.Fatalf("ID=%v, want %v %+v", got, want, contactAddress)
			} else if contactAddress.CreatedAt.IsZero() {
				t.Fatal("expected created at")
			} else if contactAddress.UpdatedAt.IsZero() {
				t.Fatal("expected updated at")
			}
			// It does not create a new db entry
			if !reflect.DeepEqual(contactAddress, contactAddressDup) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", contactAddressDup, contactAddress)
			}
		})
	})

	t.Run("FindContactAddressByID", func(t *testing.T) {
		MustCreateContactAddress(t, ctx, db, contactAddress)
		if got, err := s.FindContactAddressByID(ctx, 1); err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(got, contactAddress) {
			t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, contactAddress)
		}
	})

	t.Run("FindContactAddresses", func(t *testing.T) {
		// Differs only in address details from contactAddress
		contactAddress2 := newContactAddress("Harry", "Times Square 1", "Apartment 505", 1, 1)
		MustCreateContactAddress(t, ctx, db, contactAddress2)
		contactAddress3 := newContactAddress("Peter", "Hamburger Hafen 1", "Third floor", 2, 2)
		MustCreateContactAddress(t, ctx, db, contactAddress3)
		// Setup group memberships
		group := &sextant.Group{Name: "Superfood"}
		MustCreateGroup(t, ctx, db, group)
		membership := newGroupMembership(group, contactAddress)
		MustCreateGroupMembership(t, ctx, db, membership)
		membership2 := newGroupMembership(group, contactAddress2)
		MustCreateGroupMembership(t, ctx, db, membership2)
		// Setup tours
		tour1 := newTour("Northern Tour")
		tour1.Pickups = append(tour1.Pickups, sextant.TourItem{ContactAddress: contactAddress, State: sextant.ValidTourItemStates[0]})
		tour2 := newTour("Southern Tour")
		tour2.Riders = append(tour2.Riders, sextant.TourItem{ContactAddress: contactAddress2, State: sextant.ValidTourItemStates[0]})
		tour3 := newTour("Western Tour")
		tour3.Riders = append(tour3.Riders, sextant.TourItem{ContactAddress: contactAddress, State: sextant.ValidTourItemStates[0]})
		tour3.Deliveries = append(tour3.Deliveries, sextant.TourItem{ContactAddress: contactAddress3, State: sextant.ValidTourItemStates[0]})
		MustCreateTour(t, ctx, db, tour1)
		MustCreateTour(t, ctx, db, tour2)
		MustCreateTour(t, ctx, db, tour3)

		t.Run("FilterWithGroupID", func(t *testing.T) {
			groupID := 1
			filter := sextant.ContactAddressFilter{GroupID: &groupID}
			contactAddresses, err := s.FindContactAddresses(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(contactAddresses), 2; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, contactAddresses)
			}
			if !reflect.DeepEqual(contactAddresses[0], *contactAddress) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", contactAddresses[0], contactAddress)
			}
			if !reflect.DeepEqual(contactAddresses[1], *contactAddress2) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", contactAddresses[1], contactAddress2)
			}
		})

		t.Run("FilterWithContactName", func(t *testing.T) {
			name := "rr"
			filter := sextant.ContactAddressFilter{ContactName: &name}
			contactAddresses, err := s.FindContactAddresses(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(contactAddresses), 2; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, contactAddresses)
			}
			if !reflect.DeepEqual(contactAddresses[0], *contactAddress) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", contactAddresses[0], contactAddress)
			}
			if !reflect.DeepEqual(contactAddresses[1], *contactAddress2) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", contactAddresses[1], contactAddress2)
			}
		})

		t.Run("FilterWithAddressLabel", func(t *testing.T) {
			label := "ham"
			filter := sextant.ContactAddressFilter{AddressLabel: &label}
			contactAddresses, err := s.FindContactAddresses(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(contactAddresses), 1; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, contactAddresses)
			}
			if !reflect.DeepEqual(contactAddresses[0], *contactAddress3) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", contactAddresses[0], contactAddress3)
			}
		})

		t.Run("FilterWithGroupIDAndContactName", func(t *testing.T) {
			name := "rr"
			groupID := 1
			filter := sextant.ContactAddressFilter{ContactName: &name, GroupID: &groupID}
			contactAddresses, err := s.FindContactAddresses(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(contactAddresses), 2; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, contactAddresses)
			}
		})

		t.Run("FilterTourID", func(t *testing.T) {
			filter := sextant.ContactAddressFilter{TourID: &tour1.ID}
			contactAddresses, err := s.FindContactAddresses(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(contactAddresses), 1; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, contactAddresses)
			}
			if got, want := contactAddresses[0], *contactAddress; !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			}
		})

		t.Run("FilterTourItemtypeName", func(t *testing.T) {
			titn := "rider"
			filter := sextant.ContactAddressFilter{TourItemtypeName: &titn}
			contactAddresses, err := s.FindContactAddresses(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(contactAddresses), 2; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, contactAddresses)
			} else if got, want := contactAddresses[0], *contactAddress; !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			} else if got, want := contactAddresses[1], *contactAddress2; !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			}
		})

		t.Run("FilterTourIDs", func(t *testing.T) {
			filter := sextant.ContactAddressFilter{TourIDs: []int{tour1.ID, tour2.ID}}
			contactAddresses, err := s.FindContactAddresses(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(contactAddresses), 2; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, contactAddresses)
			} else if got, want := contactAddresses[0], *contactAddress; !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			} else if got, want := contactAddresses[1], *contactAddress2; !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			}
		})

		t.Run("FilterWithZoneID", func(t *testing.T) {
			zone := &sextant.Zone{
				Abbr:     "T",
				Name:     "Test",
				Geometry: orb.Polygon{{{2, 2}, {2, 3}, {3, 3}, {3, 2}, {2, 2}}},
			}
			MustCreateZone(t, ctx, db, zone)
			filter := sextant.ContactAddressFilter{ZoneID: &zone.ID}
			contactAddresses, err := s.FindContactAddresses(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(contactAddresses), 1; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, contactAddresses)
			}
			if !reflect.DeepEqual(contactAddresses[0], *contactAddress3) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", contactAddresses[0], contactAddress3)
			}
		})
	})
}

func MustCreateContactAddress(tb testing.TB, ctx context.Context, db *sqlite.DB, contactAddress *sextant.ContactAddress) *sextant.ContactAddress {
	tb.Helper()
	if err := sqlite.NewContactAddressService(db).FindOrCreateContactAddress(ctx, contactAddress); err != nil {
		tb.Fatal(err)
	}
	return contactAddress
}

func newContactAddress(name string, addressLabel string, addressDetails string, lat float64, lon float64) *sextant.ContactAddress {
	contact := &sextant.Contact{Name: name}
	address := &sextant.Address{
		Label:    addressLabel,
		Location: &sextant.Location{Lat: lat, Lon: lon},
	}
	return &sextant.ContactAddress{Contact: contact, Address: address, Details: addressDetails}
}

func newGroupMembership(group *sextant.Group, ca *sextant.ContactAddress) *sextant.GroupMembership {
	return &sextant.GroupMembership{GroupID: group.ID, ContactAddressID: ca.ID}
}
