package sqlite_test

import (
	"context"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestTourItemService(t *testing.T) {
	ca := newContactAddress("Harry", "Times Square 1", "Apartment 404", 1, 1)
	tour := &sextant.Tour{Name: "Northern TourItem"}

	t.Run("CreateTourItem", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		MustCreateContactAddress(t, ctx, db, ca)
		MustCreateTour(t, ctx, db, tour)

		s := sqlite.NewTourItemService(db)

		item := &sextant.TourItem{TourID: tour.ID, ContactAddressID: ca.ID, TourItemtypeID: 1, State: sextant.ValidTourItemStates[0]}
		if err := s.CreateTourItem(ctx, item); err != nil {
			t.Fatal(err)
		} else if got, want := item.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, item)
		} else if item.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if item.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}
	})

	t.Run("FindTourItemByID", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourItemService(db)

		MustCreateContactAddress(t, ctx, db, ca)
		MustCreateTour(t, ctx, db, tour)
		item := &sextant.TourItem{TourID: tour.ID, ContactAddressID: ca.ID, TourItemtypeID: 1, State: sextant.ValidTourItemStates[0]}
		MustCreateTourItem(t, ctx, db, item)
		item.ContactAddress = ca

		got, err := s.FindTourItemByID(ctx, item.ID)
		if err != nil {
			t.Fatal(err)
		} else if want := item; !reflect.DeepEqual(got, want) {
			t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got.Address, want.Address)
		}
	})

	t.Run("FindTourItemsByTourID", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourItemService(db)

		MustCreateContactAddress(t, ctx, db, ca)
		MustCreateTour(t, ctx, db, tour)
		item := &sextant.TourItem{TourID: tour.ID, ContactAddressID: ca.ID, TourItemtypeID: 1, State: sextant.ValidTourItemStates[0]}
		MustCreateTourItem(t, ctx, db, item)
		item.ContactAddress = ca

		found, err := s.FindTourItemsByTourID(ctx, tour.ID)
		if err != nil {
			t.Fatal(err)
		} else if got, want := len(found), 1; got != want {
			t.Fatalf("COUNT: %#v != %#v", got, want)
		} else if got, want := found[0], *item; !reflect.DeepEqual(got, want) {
			t.Fatalf("mismatch: %#v != %#v", got, want)
		}
	})

	t.Run("DeleteTourItem", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourItemService(db)

		MustCreateContactAddress(t, ctx, db, ca)
		MustCreateTour(t, ctx, db, tour)
		item := &sextant.TourItem{TourID: tour.ID, ContactAddressID: ca.ID, TourItemtypeID: 1, State: sextant.ValidTourItemStates[0]}
		MustCreateTourItem(t, ctx, db, item)

		if err := s.DeleteTourItem(ctx, item.ID); err != nil {
			t.Fatal(err)
		} else if _, err := s.FindTourItemByID(ctx, item.ID); err != nil && sextant.ErrorCode(err) != sextant.ENOTFOUND {
			t.Fatalf("unexpected error: %#v", err)
		}
	})

	t.Run("UpdateTourItem", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourItemService(db)

		MustCreateContactAddress(t, ctx, db, ca)
		MustCreateTour(t, ctx, db, tour)
		item := &sextant.TourItem{TourID: tour.ID, ContactAddressID: ca.ID, TourItemtypeID: 1, State: sextant.ValidTourItemStates[0]}
		MustCreateTourItem(t, ctx, db, item)
		newState := sextant.ValidTourItemStates[1]
		upd := sextant.TourItemUpdate{State: &newState}

		if updatedItem, err := s.UpdateTourItem(ctx, item.ID, upd); err != nil {
			t.Fatal(err)
		} else if got, want := updatedItem.State, newState; got != want {
			t.Fatalf("mismatch: %#v != %#v", got, want)
		}
	})
}

func MustCreateTourItem(tb testing.TB, ctx context.Context, db *sqlite.DB, item *sextant.TourItem) *sextant.TourItem {
	tb.Helper()
	if err := sqlite.NewTourItemService(db).CreateTourItem(ctx, item); err != nil {
		tb.Fatal(err)
	}
	return item
}
