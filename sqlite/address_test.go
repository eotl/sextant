package sqlite_test

import (
	"context"
	_ "fmt"
	"reflect"
	_ "strings"
	"testing"
	_ "time"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestAddressService(t *testing.T) {
	t.Run("FindOrCreateAddress", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)

		ctx := context.Background()

		s := sqlite.NewAddressService(db)
		location := &sextant.Location{Lat: 52.52150585, Lon: 13.412380949017187}
		address := &sextant.Address{Label: "Alexanderplatz 1, 12000 Berlin", Location: location}

		if err := s.FindOrCreateAddress(ctx, address); err != nil {
			t.Fatal(err)
		} else if got, want := address.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, address)
		} else if got, want := address.LocationID, 1; got != want {
			t.Fatalf("LocationID=%v, want %v", got, want)
		} else if got, want := address.Label, "Alexanderplatz 1, 12000 Berlin"; got != want {
			t.Fatalf("Label=%v, want %v", got, want)
		} else if address.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if address.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		other, err := s.FindAddressByID(ctx, 1)
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(address, other) {
			t.Fatalf("mismatch: %#v != %#v", address, other)
		}

		t.Run("WithDuplicate", func(t *testing.T) {
			address_dup := &sextant.Address{Label: "Alexanderplatz 1, Berlin", Location: location}

			if err := s.FindOrCreateAddress(ctx, address_dup); err != nil {
				t.Fatal(err)
			} else if got, want := address_dup.ID, 1; got != want {
				t.Fatalf("ID=%v, want %v %+v", got, want, address)
			} else if got, want := address_dup.Label, address.Label; got != want {
				t.Fatalf("Label=%v, want %v", got, want)
			}

			if !reflect.DeepEqual(address_dup, other) {
				t.Fatalf("mismatch: %#v != %#v", address, other)
			}
		})
	})
}

func MustCreateAddress(tb testing.TB, ctx context.Context, db *sqlite.DB, address *sextant.Address) *sextant.Address {
	tb.Helper()
	if err := sqlite.NewAddressService(db).FindOrCreateAddress(ctx, address); err != nil {
		tb.Fatal(err)
	}
	return address
}
