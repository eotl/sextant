package sqlite_test

import (
	"context"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestZoneMembershipService(t *testing.T) {
	t.Run("FindOrCreateZoneMembership", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewZoneMembershipService(db)
		zone := &sextant.Zone{Name: "Tasty-Taco"}
		location := &sextant.Location{Lon: 1, Lat: 1}
		MustCreateZone(t, ctx, db, zone)
		MustCreateLocation(t, ctx, db, location)

		zoneMembership := &sextant.ZoneMembership{ZoneID: zone.ID, LocationID: location.ID}

		if err := s.FindOrCreateZoneMembership(ctx, zoneMembership); err != nil {
			t.Fatal(err)
		} else if got, want := zoneMembership.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, zoneMembership)
		} else if zoneMembership.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if zoneMembership.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		other, err := s.FindZoneMembershipByIDs(ctx, zone.ID, location.ID)
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(zoneMembership, other) {
			t.Fatalf("mismatch: %#v != %#v", zone, other)
		}

		t.Run("WithDuplicate", func(t *testing.T) {
			membership_dup := &sextant.ZoneMembership{ZoneID: zone.ID, LocationID: location.ID}

			if err := s.FindOrCreateZoneMembership(ctx, membership_dup); err != nil {
				t.Fatal(err)
			} else if got, want := membership_dup.ID, 1; got != want {
				t.Fatalf("ID=%v, want %v %+v", got, want, membership_dup)
			}
		})
	})
}

func MustCreateZoneMembership(tb testing.TB, ctx context.Context, db *sqlite.DB, membership *sextant.ZoneMembership) *sextant.ZoneMembership {
	tb.Helper()
	if err := sqlite.NewZoneMembershipService(db).FindOrCreateZoneMembership(ctx, membership); err != nil {
		tb.Fatal(err)
	}
	return membership
}
