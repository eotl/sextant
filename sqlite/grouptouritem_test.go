package sqlite_test

import (
	"context"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestGroupTourItemService(t *testing.T) {
	t.Run("FindOrCreateGroupTourItem", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewGroupTourItemService(db)
		group := &sextant.Group{Name: "Tasty Tacos"}
		tour := &sextant.Tour{Name: "Northern Route"}
		MustCreateGroup(t, ctx, db, group)
		MustCreateTour(t, ctx, db, tour)

		item := &sextant.GroupTourItem{GroupID: group.ID, TourID: tour.ID}

		if err := s.FindOrCreateGroupTourItem(ctx, item); err != nil {
			t.Fatal(err)
		} else if got, want := item.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, item)
		} else if item.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if item.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		other, err := s.FindGroupTourItemByIDs(ctx, group.ID, tour.ID)
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(item, other) {
			t.Fatalf("mismatch: %#v != %#v", group, other)
		}

		t.Run("WithDuplicate", func(t *testing.T) {
			item_dup := &sextant.GroupTourItem{GroupID: group.ID, TourID: tour.ID}

			if err := s.FindOrCreateGroupTourItem(ctx, item_dup); err != nil {
				t.Fatal(err)
			} else if got, want := item_dup.ID, 1; got != want {
				t.Fatalf("ID=%v, want %v %+v", got, want, item_dup)
			}
		})
	})
}

func MustCreateGroupTourItem(tb testing.TB, ctx context.Context, db *sqlite.DB, item *sextant.GroupTourItem) *sextant.GroupTourItem {
	tb.Helper()
	if err := sqlite.NewGroupTourItemService(db).FindOrCreateGroupTourItem(ctx, item); err != nil {
		tb.Fatal(err)
	}
	return item
}
