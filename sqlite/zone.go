package sqlite

import (
	"context"
	_ "database/sql"
	"fmt"
	"strings"

	"eotl.supply/sextant"
	"github.com/paulmach/orb/encoding/wkb"
)

var _ sextant.ZoneService = (*ZoneService)(nil)

type ZoneService struct {
	db *DB
}

func NewZoneService(db *DB) *ZoneService {
	return &ZoneService{db: db}
}

func (s *ZoneService) CreateZone(ctx context.Context, zone *sextant.Zone) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := createZone(ctx, tx, zone); err != nil {
		return err
	}
	return tx.Commit()
}

func (s *ZoneService) DeleteZone(ctx context.Context, id int) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := deleteZone(ctx, tx, id); err != nil {
		return err
	}
	return tx.Commit()
}

func (s *ZoneService) FindZoneByAbbr(ctx context.Context, abbr string) (*sextant.Zone, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()
	zone, err := findZoneByAbbr(ctx, tx, abbr)
	if err != nil {
		return nil, err
	} else if err := attachZoneAssociations(ctx, tx, zone); err != nil {
		return nil, err
	}

	return zone, nil
}

func (s *ZoneService) FindZoneByID(ctx context.Context, id int) (*sextant.Zone, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Fetch zone object and attach owner user.
	zone, err := findZoneByID(ctx, tx, id)
	if err != nil {
		return nil, err
	} else if err := attachZoneAssociations(ctx, tx, zone); err != nil {
		return nil, err
	}

	return zone, nil
}

func (s *ZoneService) FindZones(ctx context.Context, filter sextant.ZoneFilter) ([]*sextant.Zone, int, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, 0, err
	}
	defer tx.Rollback()

	zones, n, err := findZones(ctx, tx, filter)
	if err != nil {
		return zones, n, err
	}

	for _, zone := range zones {
		if err := attachZoneAssociations(ctx, tx, zone); err != nil {
			return zones, n, err
		}
	}
	return zones, n, nil
}

func (s *ZoneService) SetZoneAbbr(zones []sextant.Zone) ([]sextant.Zone, error) {
	return nil, sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func createZone(ctx context.Context, tx *Tx, zone *sextant.Zone) error {
	zone.CreatedAt = tx.now
	zone.UpdatedAt = zone.CreatedAt

	if err := zone.Validate(); err != nil {
		return err
	}
	var err error
	result, err := tx.ExecContext(ctx, `
		INSERT INTO zones (
			parent_id,
			abbr,
			name,
			geometry,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?, ?, ?)
	`,
		zone.ParentID,
		zone.Abbr,
		zone.Name,
		wkb.Value(zone.Geometry),
		(*NullTime)(&zone.CreatedAt),
		(*NullTime)(&zone.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	zone.ID = int(id)
	zone.Children = []*sextant.Zone{}
	// Create ZoneMemberships for zone
	if err := createZoneMembershipsForZone(ctx, tx, zone); err != nil {
		return err
	}

	return nil
}

func deleteZone(ctx context.Context, tx *Tx, id int) error {
	zone, err := findZoneByID(ctx, tx, id)
	if err != nil {
		return err
	}
	if _, err := tx.ExecContext(ctx, `DELETE FROM zones WHERE id = ?`, zone.ID); err != nil {
		return FormatError(err)
	}
	return nil
}

func findZoneByAbbr(ctx context.Context, tx *Tx, abbr string) (*sextant.Zone, error) {
	zones, _, err := findZones(ctx, tx, sextant.ZoneFilter{Abbr: &abbr})
	if err != nil {
		return nil, err
	} else if len(zones) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Zone not found."}
	}
	return zones[0], nil
}

func findZoneByID(ctx context.Context, tx *Tx, id int) (*sextant.Zone, error) {
	zones, _, err := findZones(ctx, tx, sextant.ZoneFilter{ID: &id})
	if err != nil {
		return nil, err
	} else if len(zones) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Zone not found."}
	}
	return zones[0], nil
}

func findZones(ctx context.Context, tx *Tx, filter sextant.ZoneFilter) (_ []*sextant.Zone, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if v2 := filter.ParentID; v2 != nil {
		where, args = append(where, "parent_id = ?"), append(args, *v2)
	}
	if v3 := filter.Abbr; v3 != nil {
		where, args = append(where, "abbr = ?"), append(args, *v3)
	}

	rows, err := tx.QueryContext(ctx, `
		SELECT 
		    id,
				parent_id,
				abbr,
		    name,
				geometry,
		    created_at,
		    updated_at,
		    COUNT(*) OVER()
		FROM zones
		WHERE `+strings.Join(where, " AND ")+`
		ORDER BY id ASC
		`+FormatLimitOffset(filter.Limit, filter.Offset),
		args...,
	)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	// Iterate over rows and deserialize into Zone objects.
	zones := make([]*sextant.Zone, 0)
	s := wkb.GeometryScanner{}
	for rows.Next() {
		var zone sextant.Zone
		if err := rows.Scan(
			&zone.ID,
			&zone.ParentID,
			&zone.Abbr,
			&zone.Name,
			&s,
			(*NullTime)(&zone.CreatedAt),
			(*NullTime)(&zone.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		if s.Valid {
			zone.Geometry = s.Geometry
		}
		if err != nil {
			return nil, 0, err
		}
		zones = append(zones, &zone)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}
	return zones, n, nil
}

func attachZoneAssociations(ctx context.Context, tx *Tx, zone *sextant.Zone) (err error) {
	parentID := zone.ID
	filter := sextant.ZoneFilter{ParentID: &parentID}
	if zone.Children, _, err = findZones(ctx, tx, filter); err != nil {
		return fmt.Errorf("attaching zone children: %w", err)
	}
	return nil
}
