package sqlite

import (
	"context"
	_ "database/sql"
	"fmt"
	"strconv"
	"strings"

	"eotl.supply/sextant"
	"github.com/gosimple/slug"
)

var _ sextant.TourService = (*TourService)(nil)

type TourService struct {
	db *DB
}

func NewTourService(db *DB) *TourService {
	return &TourService{db: db}
}

// Creates a Tour
//
// Requirements:
// - The Name attribute must be unqiue
//
// Sideeffects:
//   - The value of Slug attribute is derived and set from Name
//   - For each referenced ContactAddress (Delivery, Pickup, Rider)
//     a TourItem is created
//   - If ContactAddress does not exist yet it is created
//   - For each refereced Group a GroupTourItem is created
func (s *TourService) CreateTour(ctx context.Context, tour *sextant.Tour) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := createTour(ctx, tx, tour); err != nil {
		return err
	}
	// Create TourItems for Tour
	// Step 1: get item types
	tourItemtypes, _, err := findTourItemtypes(ctx, tx, sextant.TourItemtypeFilter{})
	if err != nil {
		return err
	}
	// Step 2: create items per item type
	var tourItems *[]sextant.TourItem
	for _, tourItemtype := range tourItemtypes {
		switch tourItemtype.Name {
		case "delivery":
			tourItems = &tour.Deliveries
		case "pickup":
			tourItems = &tour.Pickups
		case "rider":
			tourItems = &tour.Riders
		default:
			return fmt.Errorf("Unexpected item type: %s", tourItemtype.Name)
		}
		for i, _ := range *tourItems {
			if err := findOrCreateContactAddress(ctx, tx, (*tourItems)[i].ContactAddress); err != nil {
				return err
			}
			(*tourItems)[i].TourID = tour.ID
			(*tourItems)[i].ContactAddressID = (*tourItems)[i].ContactAddress.ID
			(*tourItems)[i].TourItemtypeID = tourItemtype.ID
			(*tourItems)[i].State = sextant.ValidTourItemStates[0]
			if err := createTourItem(ctx, tx, &(*tourItems)[i]); err != nil {
				return err
			}
		}
	}
	// Create GroupTourItems
	for _, group := range tour.Groups {
		item := &sextant.GroupTourItem{GroupID: group.ID, TourID: tour.ID}
		if err := findOrCreateGroupTourItem(ctx, tx, item); err != nil {
			return err
		}
	}
	return tx.Commit()
}

// Find Tours by Filter
func (s *TourService) FindTours(ctx context.Context, filter sextant.TourFilter) ([]sextant.Tour, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()
	tours, _, err := findTours(ctx, tx, filter)
	if err != nil {
		return nil, err
	}
	for i, _ := range tours {
		if err := attachTourAssociations(ctx, tx, &tours[i]); err != nil {
			return nil, err
		}
	}
	return tours, nil
}

func (s *TourService) FindTourByIDorSlug(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
	var id int
	var slug string
	var tour *sextant.Tour

	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Parse ID or slug
	if id, err = strconv.Atoi(idOrSlug); err != nil {
		slug = idOrSlug
	}
	// Fetch tour object and attach owner user.
	if id > 0 {
		tour, err = findTourByID(ctx, tx, id)
	} else {
		tour, err = findTourBySlug(ctx, tx, slug)
	}
	if err != nil {
		return nil, err
	} else if err := attachTourAssociations(ctx, tx, tour); err != nil {
		return nil, err
	}

	return tour, nil
}

// Deletes a Tour and all its TourItems
func (s *TourService) DeleteTour(ctx context.Context, id int) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := deleteTour(ctx, tx, id); err != nil {
		return err
	}
	return tx.Commit()
}

func createTour(ctx context.Context, tx *Tx, tour *sextant.Tour) error {
	tour.CreatedAt = tx.now
	tour.UpdatedAt = tour.CreatedAt
	tour.Slug = slug.Make(tour.Name)

	if err := tour.Validate(); err != nil {
		return err
	}

	result, err := tx.ExecContext(ctx, `
		INSERT INTO tours (
			name,
			slug,
			ondate,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?, ?)
	`,
		tour.Name,
		tour.Slug,
		(*NullTime)(&tour.OnDate),
		(*NullTime)(&tour.CreatedAt),
		(*NullTime)(&tour.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	tour.ID = int(id)

	return nil
}

func findTourByID(ctx context.Context, tx *Tx, id int) (*sextant.Tour, error) {
	tours, _, err := findTours(ctx, tx, sextant.TourFilter{ID: &id})
	if err != nil {
		return nil, err
	} else if len(tours) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Tour not found."}
	}
	return &tours[0], nil
}

func findTourBySlug(ctx context.Context, tx *Tx, slug string) (*sextant.Tour, error) {
	tours, _, err := findTours(ctx, tx, sextant.TourFilter{Slug: &slug})
	if err != nil {
		return nil, err
	} else if len(tours) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Tour not found."}
	}
	return &tours[0], nil
}

func findTours(ctx context.Context, tx *Tx, filter sextant.TourFilter) (_ []sextant.Tour, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if v2 := filter.Slug; v2 != nil {
		where, args = append(where, "slug = ?"), append(args, *v2)
	}
	if v3 := filter.GroupID; v3 != nil {
		where, args = append(where, "gti.group_id = ?"), append(args, *v3)
	}
	if v4 := filter.ContactID; v4 != nil {
		where, args = append(where, "ca.contact_id = ?"), append(args, *v4)
	}

	sql := `
		SELECT
		    t.id,
		    t.name,
				t.slug,
				t.ondate,
		    t.created_at,
		    t.updated_at,
		    COUNT(*) OVER()
		FROM tours AS t`

	if filter.GroupID != nil {
		sql = sql + `
		INNER JOIN group_tour_items AS gti 
		ON gti.tour_id = t.id`
	}

	if filter.ContactID != nil {
		sql = sql + `
		INNER JOIN tour_items AS ti 
		ON ti.tour_id = t.id
		INNER JOIN contact_addresses AS ca
		ON ca.id = ti.contactaddress_id
		`
	}

	sql = sql + `
		WHERE ` + strings.Join(where, " AND ")

	sql = sql + `
		ORDER BY t.id ASC ` + FormatLimitOffset(filter.Limit, filter.Offset)

	rows, err := tx.QueryContext(ctx, sql, args...)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	// Iterate over rows and deserialize into Tour objects.
	tours := make([]sextant.Tour, 0)
	for rows.Next() {
		var tour sextant.Tour
		if err := rows.Scan(
			&tour.ID,
			&tour.Name,
			&tour.Slug,
			(*NullTime)(&tour.OnDate),
			(*NullTime)(&tour.CreatedAt),
			(*NullTime)(&tour.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		tours = append(tours, tour)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return tours, n, nil
}

func deleteTour(ctx context.Context, tx *Tx, id int) error {
	tour, err := findTourByID(ctx, tx, id)
	if err != nil {
		return err
	}
	if _, err := tx.ExecContext(ctx, `DELETE FROM tours WHERE id = ?`, tour.ID); err != nil {
		return FormatError(err)
	}
	return nil
}

func attachTourAssociations(ctx context.Context, tx *Tx, tour *sextant.Tour) (err error) {
	// Attach TourItems
	tourItemtypes, _, err := findTourItemtypes(ctx, tx, sextant.TourItemtypeFilter{})
	if err != nil {
		return err
	}
	for _, t := range tourItemtypes {
		filter := sextant.TourItemFilter{TourID: &tour.ID, TourItemtypeID: &t.ID}
		if items, _, err := findTourItems(ctx, tx, filter); err != nil {
			return fmt.Errorf("attach tour items: %w", err)
		} else {
			for i, _ := range items {
				if err := attachTourItemAssociations(ctx, tx, &items[i]); err != nil {
					return err
				}
			}
			switch t.Name {
			case "delivery":
				tour.Deliveries = items
			case "pickup":
				tour.Pickups = items
			case "rider":
				tour.Riders = items
			default:
				return fmt.Errorf("Unexpected item type: %s", t.Name)
			}
		}
	}
	// Attach Groups
	filter := sextant.GroupFilter{TourID: &tour.ID}
	if groups, _, err := findGroups(ctx, tx, filter); err != nil {
		return fmt.Errorf("attach groups: %w", err)
	} else {
		tour.Groups = groups
	}
	return nil
}
