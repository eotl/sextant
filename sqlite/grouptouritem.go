package sqlite

import (
	"context"
	_ "database/sql"
	"errors"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.GroupTourItemService = (*GroupTourItemService)(nil)

type GroupTourItemService struct {
	db *DB
}

func NewGroupTourItemService(db *DB) *GroupTourItemService {
	return &GroupTourItemService{db: db}
}

func (s *GroupTourItemService) FindOrCreateGroupTourItem(ctx context.Context, item *sextant.GroupTourItem) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := findOrCreateGroupTourItem(ctx, tx, item); err != nil {
		return err
	}
	return tx.Commit()
}

func (s *GroupTourItemService) FindGroupTourItemByIDs(ctx context.Context, groupID int, tourID int) (*sextant.GroupTourItem, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	item, err := findGroupTourItemByIDs(ctx, tx, groupID, tourID)
	if err != nil {
		return nil, err
	} else if err := attachGroupTourItemAssociations(ctx, tx, item); err != nil {
		return nil, err
	}

	return item, nil
}

func findOrCreateGroupTourItem(ctx context.Context, tx *Tx, item *sextant.GroupTourItem) error {
	itemFound, err := findGroupTourItemByIDs(ctx, tx, item.GroupID, item.TourID)
	if err != nil {
		if err := createGroupTourItem(ctx, tx, item); err != nil {
			return err
		}
		return nil
	}
	*item = *itemFound

	return nil

}

func createGroupTourItem(ctx context.Context, tx *Tx, item *sextant.GroupTourItem) error {
	item.CreatedAt = tx.now
	item.UpdatedAt = item.CreatedAt

	if err := item.Validate(); err != nil {
		return err
	}

	result, err := tx.ExecContext(ctx, `
		INSERT INTO group_tour_items (
			group_id,
			tour_id,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?)
	`,
		item.GroupID,
		item.TourID,
		(*NullTime)(&item.CreatedAt),
		(*NullTime)(&item.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	item.ID = int(id)

	return nil
}

func findGroupTourItemByIDs(ctx context.Context, tx *Tx, groupID int, tourID int) (*sextant.GroupTourItem, error) {
	filter := sextant.GroupTourItemFilter{GroupID: &groupID, TourID: &tourID}
	items, _, err := findGroupTourItems(ctx, tx, filter)
	if err != nil {
		return nil, err
	} else if len(items) == 0 {
		return nil, errors.New("GroupTourItem not found")
	}
	return items[0], nil
}

func findGroupTourItems(ctx context.Context, tx *Tx, filter sextant.GroupTourItemFilter) (_ []*sextant.GroupTourItem, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.GroupID; v != nil {
		where, args = append(where, "group_id = ?"), append(args, *v)
	}
	if v2 := filter.TourID; v2 != nil {
		where, args = append(where, "tour_id = ?"), append(args, *v2)
	}

	rows, err := tx.QueryContext(ctx, `
		SELECT 
		    id,
				group_id,
				tour_id,
		    created_at,
		    updated_at,
		    COUNT(*) OVER()
		FROM group_tour_items
		WHERE `+strings.Join(where, " AND ")+`
		ORDER BY id ASC
		`+FormatLimitOffset(filter.Limit, filter.Offset),
		args...,
	)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	items := make([]*sextant.GroupTourItem, 0)
	for rows.Next() {
		var item sextant.GroupTourItem
		if err := rows.Scan(
			&item.ID,
			&item.GroupID,
			&item.TourID,
			(*NullTime)(&item.CreatedAt),
			(*NullTime)(&item.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		items = append(items, &item)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return items, n, nil
}

func attachGroupTourItemAssociations(ctx context.Context, tx *Tx, item *sextant.GroupTourItem) (err error) {
	return nil
}
