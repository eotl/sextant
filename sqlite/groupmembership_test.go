package sqlite_test

import (
	"context"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestGroupMembershipService(t *testing.T) {
	t.Run("FindOrCreateGroupMembership", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewGroupMembershipService(db)
		group := &sextant.Group{Name: "Tasty-Taco"}
		contactAddress := &sextant.ContactAddress{
			Contact: &sextant.Contact{Name: "Harry"},
			Address: &sextant.Address{
				Label:    "Times Square 1",
				Location: &sextant.Location{Lat: 1, Lon: 1, Words: ""},
			},
		}
		MustCreateGroup(t, ctx, db, group)
		MustCreateContactAddress(t, ctx, db, contactAddress)

		groupMembership := &sextant.GroupMembership{GroupID: group.ID, ContactAddressID: contactAddress.ID}

		if err := s.FindOrCreateGroupMembership(ctx, groupMembership); err != nil {
			t.Fatal(err)
		} else if got, want := groupMembership.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, groupMembership)
		} else if groupMembership.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if groupMembership.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		other, err := s.FindGroupMembershipByIDs(ctx, group.ID, contactAddress.ID)
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(groupMembership, other) {
			t.Fatalf("mismatch: %#v != %#v", group, other)
		}

		t.Run("WithDuplicate", func(t *testing.T) {
			membership_dup := &sextant.GroupMembership{GroupID: group.ID, ContactAddressID: contactAddress.ID}

			if err := s.FindOrCreateGroupMembership(ctx, membership_dup); err != nil {
				t.Fatal(err)
			} else if got, want := membership_dup.ID, 1; got != want {
				t.Fatalf("ID=%v, want %v %+v", got, want, membership_dup)
			}
		})
	})
}

func MustCreateGroupMembership(tb testing.TB, ctx context.Context, db *sqlite.DB, membership *sextant.GroupMembership) *sextant.GroupMembership {
	tb.Helper()
	if err := sqlite.NewGroupMembershipService(db).FindOrCreateGroupMembership(ctx, membership); err != nil {
		tb.Fatal(err)
	}
	return membership
}
