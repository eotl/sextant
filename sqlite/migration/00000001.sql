CREATE TABLE groups (
	id          INTEGER PRIMARY KEY AUTOINCREMENT,
	name			  TEXT NOT NULL UNIQUE,
	slug			  TEXT NOT NULL UNIQUE,
	created_at  TEXT NOT NULL,
	updated_at  TEXT NOT NULL
);

CREATE TABLE group_memberships (
	id                  INTEGER PRIMARY KEY AUTOINCREMENT,
	group_id            INTEGER NOT NULL REFERENCES groups (id) ON DELETE CASCADE,
	contact_address_id  INTEGER NOT NULL REFERENCES contact_addresses (id) ON DELETE CASCADE,
	created_at          TEXT NOT NULL,
	updated_at          TEXT NOT NULL,

	UNIQUE(group_id, contact_address_id)
);

CREATE TABLE group_tour_items (
	id                  INTEGER PRIMARY KEY AUTOINCREMENT,
	group_id            INTEGER NOT NULL REFERENCES groups (id) ON DELETE CASCADE,
	tour_id             INTEGER NOT NULL REFERENCES tours (id) ON DELETE CASCADE,
	created_at          TEXT NOT NULL,
	updated_at          TEXT NOT NULL,

	UNIQUE(group_id, tour_id)
);
