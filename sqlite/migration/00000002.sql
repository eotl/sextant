CREATE TABLE zones (
  id          INTEGER PRIMARY KEY AUTOINCREMENT,
  parent_id   INTEGER REFERENCES zones (id),
  abbr        TEXT NOT NULL UNIQUE,
  name        TEXT NOT NULL UNIQUE,
	geometry		BLOB,
  created_at  TEXT NOT NULL,
  updated_at  TEXT NOT NULL,

	CHECK(id <> parent_id)
);

CREATE TABLE zone_memberships (
  id          INTEGER PRIMARY KEY AUTOINCREMENT,
  zone_id     INTEGER NOT NULL REFERENCES zones (id) ON DELETE CASCADE,
  location_id INTEGER NOT NULL REFERENCES locations (id) ON DELETE CASCADE,
  created_at  TEXT NOT NULL,
  updated_at  TEXT NOT NULL,
	
	UNIQUE(zone_id, location_id)
);
