package sqlite

import (
	"context"
	_ "database/sql"
	"fmt"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.ContactAddressService = (*ContactAddressService)(nil)

type ContactAddressService struct {
	db *DB
}

func NewContactAddressService(db *DB) *ContactAddressService {
	return &ContactAddressService{db: db}
}

func (s *ContactAddressService) FindContactAddresses(ctx context.Context, filter sextant.ContactAddressFilter) ([]sextant.ContactAddress, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	contactAddresses, _, err := findContactAddresses(ctx, tx, filter)
	if err != nil {
		return nil, err
	}

	for i, _ := range contactAddresses {
		if err := attachContactAddressAssociations(ctx, tx, &contactAddresses[i]); err != nil {
			return nil, err
		}
	}

	return contactAddresses, nil
}

func (s *ContactAddressService) FindContactAddressByID(ctx context.Context, id int) (*sextant.ContactAddress, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Fetch ca object and attach owner user.
	ca, err := findContactAddressByID(ctx, tx, id)
	if err != nil {
		return nil, err
	} else if err := attachContactAddressAssociations(ctx, tx, ca); err != nil {
		return nil, err
	}

	return ca, nil
}

func (s *ContactAddressService) FindOrCreateContactAddress(ctx context.Context, contactAddress *sextant.ContactAddress) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := findOrCreateContactAddress(ctx, tx, contactAddress); err != nil {
		return err
	}
	return tx.Commit()
}

func findOrCreateContactAddress(ctx context.Context, tx *Tx, contactAddress *sextant.ContactAddress) error {
	if err := findOrCreateAddress(ctx, tx, contactAddress.Address); err != nil {
		return err
	}
	contactAddress.AddressID = contactAddress.Address.ID
	if err := findOrCreateContact(ctx, tx, contactAddress.Contact); err != nil {
		return err
	}
	contactAddress.ContactID = contactAddress.Contact.ID

	filter := sextant.ContactAddressFilter{
		ContactID: &contactAddress.ContactID,
		AddressID: &contactAddress.AddressID,
		Details:   &contactAddress.Details,
	}
	contactAddresses, _, err := findContactAddresses(ctx, tx, filter)
	if err != nil {
		return err
	} else if len(contactAddresses) == 0 {
		if err := createContactAddress(ctx, tx, contactAddress); err != nil {
			return err
		}
		return nil
	}
	contactAddressFound := contactAddresses[0]
	contactAddressFound.Contact = contactAddress.Contact
	contactAddressFound.Address = contactAddress.Address
	*contactAddress = contactAddressFound
	return nil
}

func createContactAddress(ctx context.Context, tx *Tx, contactAddress *sextant.ContactAddress) error {
	contactAddress.CreatedAt = tx.now
	contactAddress.UpdatedAt = contactAddress.CreatedAt

	if err := contactAddress.Validate(); err != nil {
		return err
	}

	result, err := tx.ExecContext(ctx, `
		INSERT INTO contact_addresses (
			contact_id,
			address_id,
			details,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?, ?)
	`,
		contactAddress.ContactID,
		contactAddress.AddressID,
		contactAddress.Details,
		(*NullTime)(&contactAddress.CreatedAt),
		(*NullTime)(&contactAddress.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	contactAddress.ID = int(id)

	return nil
}

func findContactAddressByID(ctx context.Context, tx *Tx, id int) (*sextant.ContactAddress, error) {
	cas, _, err := findContactAddresses(ctx, tx, sextant.ContactAddressFilter{ID: &id})
	if err != nil {
		return nil, err
	} else if len(cas) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "ContactAddress not found."}
	}
	return &cas[0], nil
}

func findContactAddresses(ctx context.Context, tx *Tx, filter sextant.ContactAddressFilter) (_ []sextant.ContactAddress, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	whereFuzzy, args := []string{}, []interface{}{}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if v2 := filter.ContactID; v2 != nil {
		where, args = append(where, "contact_id = ?"), append(args, *v2)
	}
	if v3 := filter.AddressID; v3 != nil {
		where, args = append(where, "address_id = ?"), append(args, *v3)
	}
	if v4 := filter.Details; v4 != nil {
		where, args = append(where, "details = ?"), append(args, *v4)
	}
	if v5 := filter.GroupID; v5 != nil {
		where, args = append(where, "gm.group_id = ?"), append(args, *v5)
	}
	if v6 := filter.ContactName; v6 != nil {
		whereFuzzy, args = append(whereFuzzy, "c.name LIKE ?"), append(args, fmt.Sprintf("%%%s%%", *v6))
	}
	if v7 := filter.AddressLabel; v7 != nil {
		whereFuzzy, args = append(whereFuzzy, "a.label LIKE ?"), append(args, fmt.Sprintf("%%%s%%", *v7))
	}
	if v8 := filter.ZoneID; v8 != nil {
		where, args = append(where, "zm.zone_id = ?"), append(args, *v8)
	}
	if v9 := filter.TourID; v9 != nil {
		where, args = append(where, "ti.tour_id = ?"), append(args, *v9)
	}
	if v10 := filter.TourItemtypeName; v10 != nil {
		where, args = append(where, "tit.name = ?"), append(args, *v10)
	}
	if v11 := filter.TourIDs; v11 != nil {
		// Construct a place holder for value list
		// as part of the sql IN operator
		// e.g. "?, ?, ?"
		ph := make([]string, 0, len(v11))
		for i := 0; i < len(v11); i++ {
			ph = append(ph, "?")
		}
		phStr := strings.Join(ph, ", ")
		whereCond := fmt.Sprintf("ti.tour_id IN (%s)", phStr)
		where = append(where, whereCond)
		// Add args
		for _, i := range v11 {
			args = append(args, i)
		}
		// DISCUSS:
		// As an alternative we could use OR statements.
		// However, sql query might get very long
	}

	sql := `
		SELECT 
			ca.id,
			ca.contact_id,
			ca.address_id,
			ca.details,
			ca.created_at,
			ca.updated_at,
			COUNT(*) OVER()
		FROM contact_addresses AS ca`

	if filter.GroupID != nil {
		sql = sql + `
		INNER JOIN group_memberships AS gm
		ON gm.contact_address_id = ca.id`
	}

	if filter.ContactName != nil {
		sql = sql + `
		INNER JOIN contacts AS c 
		ON c.id = ca.contact_id`
	}

	if filter.AddressLabel != nil {
		sql = sql + `
		INNER JOIN addresses AS a 
		ON a.id = ca.address_id`
	}

	if filter.ZoneID != nil {
		if filter.AddressLabel == nil {
			sql = sql + `
			INNER JOIN addresses AS a 
			ON a.id = ca.address_id`
		}
		sql = sql + `
		INNER JOIN zone_memberships AS zm
		ON zm.location_id = a.location_id`
	}

	if filter.TourID != nil || filter.TourItemtypeName != nil || filter.TourIDs != nil {
		sql = sql + `
		INNER JOIN tour_items AS ti 
		ON ti.contactaddress_id = ca.id`
	}

	if filter.TourItemtypeName != nil {
		sql = sql + `
		INNER JOIN tour_itemtypes AS tit 
		ON tit.id = ti.touritemtype_id`
	}

	sql = sql + `
		WHERE ` + strings.Join(where, " AND ")

	if len(whereFuzzy) > 0 {
		sql = sql + ` AND (` + strings.Join(whereFuzzy, " OR ") + `)`
	}

	sql = sql + `
		ORDER BY ca.id ASC ` + FormatLimitOffset(filter.Limit, filter.Offset)

	rows, err := tx.QueryContext(ctx, sql, args...)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	// Iterate over rows and deserialize into ContactAddress objects.
	contactAddresses := make([]sextant.ContactAddress, 0)
	for rows.Next() {
		var contactAddress sextant.ContactAddress
		if err := rows.Scan(
			&contactAddress.ID,
			&contactAddress.ContactID,
			&contactAddress.AddressID,
			&contactAddress.Details,
			(*NullTime)(&contactAddress.CreatedAt),
			(*NullTime)(&contactAddress.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		contactAddresses = append(contactAddresses, contactAddress)
	}

	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return contactAddresses, n, nil
}

func attachContactAddressAssociations(ctx context.Context, tx *Tx, ca *sextant.ContactAddress) (err error) {
	if ca.Contact, err = findContactByID(ctx, tx, ca.ContactID); err != nil {
		return fmt.Errorf("attach contact: %w", err)
	}
	if ca.Address, err = findAddressByID(ctx, tx, ca.AddressID); err != nil {
		return fmt.Errorf("attach address: %w", err)
	}
	if ca.Location, err = findLocationByID(ctx, tx, ca.LocationID); err != nil {
		return fmt.Errorf("attach location: %w", err)
	}
	return nil
}
