package sextant

import (
	"encoding/json"
	"time"
)

const (
	TimeLayout = "02-Jan-06 15:04"
)

type TimeWindow struct {
	Start time.Time `json:"-"`
	End   time.Time `json:"-"`
}

func (t *TimeWindow) MarshalJSON() ([]byte, error) {
	a := [2]int64{t.Start.Unix(), t.End.Unix()}
	return json.Marshal(a)
}

func (t *TimeWindow) UnmarshalJSON(data []byte) error {
	var values []int64
	err := json.Unmarshal(data, &values)
	if err != nil {
		return err
	}
	t.Start = time.Unix(values[0], 0).UTC()
	t.End = time.Unix(values[1], 0).UTC()
	return nil
}

type TimeWindowService interface {
	Create(start string, end string) (*TimeWindow, error)
}
