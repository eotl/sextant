package sextant

type FileService interface {
	ToJSON(filename string, data interface{}, path string) (bool, error)

	GetFileNameWithoutSuffix(path string) string

	SetFirstLine(filepath string, header string, action string) (string, error)
}
