package sextant

import (
	"context"
	"time"
)

type GroupTourItem struct {
	ID        int
	GroupID   int
	TourID    int
	CreatedAt time.Time
	UpdatedAt time.Time
}

type GroupTourItemFilter struct {
	ID      *int
	GroupID *int
	TourID  *int
	Offset  int
	Limit   int
}

func (g *GroupTourItem) Validate() error {
	return nil
}

type GroupTourItemService interface {
	FindGroupTourItemByIDs(ctx context.Context, groupID int, tourID int) (*GroupTourItem, error)

	FindOrCreateGroupTourItem(ctx context.Context, item *GroupTourItem) error
}
