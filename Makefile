DEFAULT := dev

dev: theme ui build config data-testing groups

REQS       := go tar git curl jq
REQS_CHECK := \
	$(foreach exec,$(REQS),\
	$(if $(shell which $(exec)),$(exec) found,$(error "$(exec) is missing, please install")))

THEME_BOOTSTRAP_URL := https://registry.npmjs.org/@eotl/theme-bootstrap/latest
THEME_PKG_VERSION   := $(shell curl -s $(THEME_BOOTSTRAP_URL) | jq -r '.version')
THEME_URL           := https://registry.npmjs.org/@eotl/theme-bootstrap/-/theme-bootstrap-${THEME_PKG_VERSION}.tgz
WWW_PATH            := $(shell pwd)/www/dist
TMP_THEME_PATH      := /tmp/theme-eotl.tar.gz

theme-download:
	@if [ ! -f $(TMP_THEME_PATH) ]; then \
		echo "Downloading theme files..." && \
		curl -s "$(THEME_URL)" > $(TMP_THEME_PATH); \
	else \
		echo "$(TMP_THEME_PATH) exists already, skipping..."; \
	fi

theme-unzip:
	@if [ ! -d $(WWW_PATH) ]; then \
		echo "Copying theme files into place..." && \
		mkdir -p "$(WWW_PATH)" && \
		cd $(WWW_PATH) && \
		tar -xf "$(TMP_THEME_PATH)" --strip-components 2 package/dist; \
		echo "Deleting $(TMP_THEME_PATH)" && \
		rm $(TMP_THEME_PATH); \
	else \
		echo "$(WWW_PATH) exists, skip unziping theme..."; \
	fi

theme-images:
	@rm -rf "$(WWW_PATH)/images/icons"
	@if [ -d $(WWW_PATH)/icons ]; then \
		echo "Copying theme images into place..." && \
		mv "$(WWW_PATH)/icons" "$(WWW_PATH)/images/"; \
	else \
		echo "$(WWW_PATH)/icons exists already, skipping..."; \
	fi

theme-index:
	@if [ -f $(WWW_PATH)/index.html ]; then \
		rm $(WWW_PATH)/index.html; \
	else \
		echo "$(WWW_PATH)/index.html exists already, skipping..."; \
	fi

theme: theme-download theme-unzip theme-images theme-index

UI_PKG_VERSION := $(shell curl -s https://registry.npmjs.org/@eotl/sextant/latest | jq -r '.version')
UI_URL         := https://registry.npmjs.org/@eotl/sextant/-/sextant-$(UI_PKG_VERSION).tgz
TMP_UI_PATH    := /tmp/ui-eotl.tar.gz

ui-download:
	@if [ ! -f $(TMP_UI_PATH) ]; then \
		echo "Downloading UI files..." && \
		curl -s "$(UI_URL)" > $(TMP_UI_PATH); \
	else \
		echo "$(TMP_UI_PATH) exists already, skipping..."; \
	fi

ui-unzip:
	@if [ ! -f "./www/dist/index.html" ]; then \
		echo "Copying UI files into place..." && \
		mkdir -p "$(WWW_PATH)" && \
		cd $(WWW_PATH) && \
		tar -xf "$(TMP_UI_PATH)" --strip-components 2 package/dist; \
		echo "Deleting $(TMP_UI_PATH)" && \
		rm $(TMP_UI_PATH); \
	fi

ui: ui-download ui-unzip

GO_MAJOR_VERSION = $(shell go version | cut -c 14- | cut -d' ' -f1 | cut -d'.' -f1)
GO_MINOR_VERSION = $(shell go version | cut -c 14- | cut -d' ' -f1 | cut -d'.' -f2)
MINIMUM_SUPPORTED_GO_MAJOR_VERSION = 1
MINIMUM_SUPPORTED_GO_MINOR_VERSION = 21
GO_VERSION_VALIDATION_ERR_MSG = \
	Golang version is not supported, please update to at least $(MINIMUM_SUPPORTED_GO_MAJOR_VERSION).$(MINIMUM_SUPPORTED_GO_MINOR_VERSION)

validate-go-version:
	@if [ $(GO_MAJOR_VERSION) -gt $(MINIMUM_SUPPORTED_GO_MAJOR_VERSION) ]; then \
		exit 0 ;\
	elif [ $(GO_MAJOR_VERSION) -lt $(MINIMUM_SUPPORTED_GO_MAJOR_VERSION) ]; then \
		echo '$(GO_VERSION_VALIDATION_ERR_MSG)';\
		exit 1; \
	elif [ $(GO_MINOR_VERSION) -lt $(MINIMUM_SUPPORTED_GO_MINOR_VERSION) ] ; then \
		echo '$(GO_VERSION_VALIDATION_ERR_MSG)';\
		exit 1; \
	fi

SEXTANT      := ./cmd/sextant
SEXTANT_BIN  := ./sextant
SEXTANTD     := ./cmd/sextantd
SEXTANTD_BIN := ./sextantd
VERSION      := $(shell git rev-list -1 HEAD)
LDFLAGS      := "-X 'main.Version=$(VERSION)'"
DIST_LDFLAGS := $(LDFLAGS)" -s -w"

build-sextant:
	@echo "Building sextant..." && \
		go build -ldflags=$(LDFLAGS) -tags="$(TAGS)" $(SEXTANT)

build-sextantd:
	@echo "Building sextantd..." && \
		go build -ldflags=$(LDFLAGS) -tags="$(TAGS)" $(SEXTANTD)

build: validate-go-version build-sextant build-sextantd

test:
	@go test -v ./...

lint:
	@test -z $(gofmt -l .)

CONF_PATH := ${HOME}/.sextant.conf

config:
	@if [ ! -f "${CONF_PATH}" ]; then \
		echo "Copying default config into place..." && \
		cp ./sextant.conf $(CONF_PATH); \
	fi

DATA_PATH   := ${HOME}/.local/share/eotl/core/data
ZONES_URL   := https://codeberg.org/eotl/data
ZONES_PATH  := ${HOME}/.local/share/eotl/core/data/zones
SQLITE_PATH	:= ${HOME}/.sextant.db
SQLITE_SHM  := ${HOME}/.sextant.db-shm
SQLITE_WAL  := ${HOME}/.sextant.db-wal

clear-sqlite-db:
	@if [ -f "${SQLITE_PATH}" ]; then \
		rm ${SQLITE_PATH}; \
	fi
	@if [ -f "${SQLITE_SHM}" ]; then \
		rm ${SQLITE_SHM}; \
	fi
	@if [ -f "${SQLITE_WAL}" ]; then \
		rm ${SQLITE_WAL}; \
	fi

clone-data-repo:
	@if [ ! -d "${DATA_PATH}" ]; then \
		echo "Loading zones data..." && \
		mkdir -p $(DATA_PATH) && \
		cd $(DATA_PATH) && \
		git init && \
		git remote add -f origin "$(ZONES_URL)" && \
		git sparse-checkout init && \
		git sparse-checkout set "zones" && \
		git pull --depth=1 origin main; \
	fi

load-data-places:
	@echo "Ensuring Places data is loaded..." && \

load-data-testing:
	@curl -d @www/testing/riders.json http://localhost:8081/api/contacts && \
		curl -d @www/testing/customers.json http://localhost:8081/api/contacts && \
		curl -d @www/testing/suppliers.json http://localhost:8081/api/contacts

load-data-zones:
	@echo "Ensuring Zones data is loaded..." && \
	$(SEXTANT_BIN) zone create -f "${ZONES_PATH}/berlin.geojson" -n GEN && \
	$(SEXTANT_BIN) zone create -f "${ZONES_PATH}/berlin-districts.geojson" -p 1  && \
	$(SEXTANT_BIN) zone create -f "${ZONES_PATH}/berlin-district-kreuzberg.geojson" -p 9  && \
	$(SEXTANT_BIN) zone create -f "${ZONES_PATH}/berlin-district-neukoelln.geojson" -p 3

data: clone-data-repo load-data-places load-data-zones

data-testing: clone-data-repo load-data-testing load-data-zones


rebuild-db: clear-sqlite-db data

groups:
	@$(SEXTANT_BIN) group list | grep -q riders || \
		(echo "Creating default groups..."; \
			$(SEXTANT_BIN) group create contacts 2>&1; \
			$(SEXTANT_BIN) group create riders 2>&1; \
			$(SEXTANT_BIN) group create suppliers 2>&1; \
			$(SEXTANT_BIN) group create customers 2>&1;)

run:
	@$(SEXTANTD_BIN) -v

IMAGE := codeberg.org/eotl/sextant
TAG   := latest # override with `make docker-build TAG=...`

docker-build:
	@docker build -t $(IMAGE):$(TAG) .

docker-push:
	@docker push $(IMAGE):$(TAG)

docker: docker-build docker-push

release-reqs:
	@which goreleaser >/dev/null 2>&1 || (echo "ERROR: goreleaser is required."; exit 1)

snapshot: release-reqs
	@goreleaser build --snapshot --single-target --clean

release: release-reqs
	@goreleaser release --clean

release-check:
	@goreleaser check

ci: theme ui

bundle:
	@go install github.com/go-bindata/go-bindata/v3/go-bindata@latest && \
	go-bindata -o http/bindata.go -pkg http -prefix "www/dist" -fs ./www/dist/...
