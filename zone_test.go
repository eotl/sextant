package sextant

import (
	"fmt"
	"io/ioutil"
	"reflect"
	"testing"

	"github.com/paulmach/orb"
)

func TestZone(t *testing.T) {

	t.Run("ToFeatureCollection", func(t *testing.T) {
		zone := Zone{
			Abbr: "MTT",
			Name: "Mitte",
			Geometry: orb.MultiPolygon{
				{{{0, 0}, {0, 2}, {2, 2}, {2, 0}, {0, 0}}},
				{{{1, 1}, {1, 3}, {3, 3}, {3, 1}, {1, 1}}},
			},
		}
		zoneWithChildren := Zone{
			Abbr: "BER",
			Name: "Berlin",
			Geometry: orb.MultiPolygon{
				{{{0, 0}, {0, 2}, {2, 2}, {2, 0}, {0, 0}}},
				{{{1, 1}, {1, 3}, {3, 3}, {3, 1}, {1, 1}}},
			},
			Children: []*Zone{&zone},
		}

		tables := []struct {
			name     string
			input    Zone
			want     string
			want_err string
		}{
			{"NoChildren", zone, "testdata/want/zone.json", ""},
			{"WithChildren", zoneWithChildren, "testdata/want/zone-with-children.json", ""},
		}

		for _, table := range tables {
			t.Run(table.name, func(t *testing.T) {
				got, got_err := table.input.ToFeatureCollection().MarshalJSON()
				if got_err != nil {
					if got_err.Error() != table.want_err {
						t.Errorf("EncodeZone(%v) -> got %+v want %+v", table.input, got_err, table.want_err)
					}
				} else if wantBytes, err := ioutil.ReadFile(table.want); err != nil {
					t.Fatal(err)
				} else {
					gotStr := string(got)
					wantStr := string(wantBytes)
					if !reflect.DeepEqual(gotStr, wantStr) {
						t.Errorf("EncodeZone(%v) -> \ngot \n%#v \nwant \n%#v", table.input, gotStr, wantStr)
					}
				}
			})
		}
	})

	t.Run("Contains", func(t *testing.T) {
		p := orb.Polygon{
			{{0, 0}, {0, 2}, {2, 2}, {2, 0}, {0, 0}},
		}
		mp := orb.MultiPolygon{
			{{{0, 0}, {0, 2}, {2, 2}, {2, 0}, {0, 0}}},
			{{{1, 1}, {1, 3}, {3, 3}, {3, 1}, {1, 1}}},
		}
		zoneP := Zone{Geometry: p}
		zoneMP := Zone{Geometry: mp}

		tcs := []struct {
			inputZone     Zone
			inputLocation Location
			want          bool
		}{
			{zoneP, Location{Lon: 0.5, Lat: 0.5}, true},
			{zoneP, Location{Lon: 3, Lat: 3}, false},
			{zoneMP, Location{Lon: 0.5, Lat: 0.5}, true},
			{zoneMP, Location{Lon: 3, Lat: 3}, true},
		}

		for _, tc := range tcs {
			geoType := fmt.Sprintf("%T", tc.inputZone.Geometry)
			t.Run(geoType, func(t *testing.T) {
				got := tc.inputZone.Contains(tc.inputLocation)
				if got != tc.want {
					t.Errorf("Contains() -> got %v want %v", got, tc.want)
				}
			})
		}
	})

}
