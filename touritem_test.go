package sextant

import (
	"reflect"
	"testing"
)

func TestTourItem(t *testing.T) {
	t.Run("Validate()", func(t *testing.T) {
		tcs := []struct {
			name  string
			input string
			want  error
		}{
			{"State Missing", "", Errorf("invalid", "TourItem state missing")},
			{"Valid State", "created", nil},
			{"Invalid State", "xxx", Errorf("invalid", "TourItem state invalid")},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				ti := TourItem{State: tc.input}
				if got, want := ti.Validate(), tc.want; !reflect.DeepEqual(got, want) {
					t.Errorf("Validate() -> got %v want %v", got, want)
				}
			})
		}
	})
}
