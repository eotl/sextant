package sextant

import (
	"testing"
)

func TestTour(t *testing.T) {
	t.Run("StopsCount", func(t *testing.T) {
		tour := Tour{
			Deliveries: []TourItem{TourItem{}},
			Pickups:    []TourItem{TourItem{}},
		}
		if got, want := tour.StopsCount(), 2; got != want {
			t.Errorf("StopsCount() -> got %v want %v", got, want)
		}
	})
}
