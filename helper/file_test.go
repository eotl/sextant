package helper

import (
	"testing"
)

func TestFileHasLine(t *testing.T) {
	goodFilepath := "testdata/input/file.txt"
	tcs := []struct {
		inputFilepath string
		inputLine     string
		want          bool
		wantErr       error
	}{
		{goodFilepath, "Charly is happy today!", true, nil},
		{goodFilepath, "Sally is sad :-(", false, nil},
	}

	for _, tc := range tcs {
		got, gotErr := FileHasLine(tc.inputFilepath, tc.inputLine)
		if gotErr != tc.wantErr {
			t.Errorf("FileHasLine() -> gotErr: %v wantErr: %v", gotErr, tc.wantErr)
		} else if got != tc.want {
			t.Errorf("FileHasLine() -> got: %v want: %v", got, tc.want)
		}
	}
}
