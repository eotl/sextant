package helper

import (
	"github.com/sergi/go-diff/diffmatchpatch"
	"io/ioutil"
	"log"
	"os"
)

// Please remove tmp file after usage
func MkTempFile(s string) string {
	tmpFile, err := ioutil.TempFile(os.TempDir(), "sextant-go-test-")
	if err != nil {
		log.Fatal("Cannot create temporary file", err)
	}
	text := []byte(s)
	if _, err = tmpFile.Write(text); err != nil {
		log.Fatal("Failed to write to temporary file", err)
	}
	if err := tmpFile.Close(); err != nil {
		log.Fatal(err)
	}
	return tmpFile.Name()
}

func DiffText(text1 string, text2 string) string {
	dmp := diffmatchpatch.New()
	diffs := dmp.DiffMain(text1, text2, false)
	return dmp.DiffPrettyText(diffs)
}
