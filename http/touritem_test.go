package http_test

import (
	"context"
	//"io"
	"net/http"
	"strings"
	"testing"

	"eotl.supply/sextant"
	//"eotl.supply/sextant/helper"
	"github.com/gavv/httpexpect/v2"
)

func TestTourItemCreate(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Mock TourItemtype db lookup
	s.TourItemtypeService.FindTourItemtypeByNameFn = func(ctx context.Context, name string) (*sextant.TourItemtype, error) {
		tit := &sextant.TourItemtype{ID: 1, Name: name}
		return tit, nil
	}

	// Mock TourItemService.CreateTourItem
	s.TourItemService.CreateTourItemFn = func(ctx context.Context, ti *sextant.TourItem) error {
		return nil
	}

	e := httpexpect.New(t, s.URL())
	input := "testdata/input/touritem.json"

	req := e.POST("/api/touritems")
	req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
	if inputData, err := readFile(input); err != nil {
		t.Fatal(err)
	} else {
		req.WithBytes([]byte(inputData))

		resp := req.Expect()
		resp.Status(http.StatusOK)
		obj := resp.JSON().Object()
		obj.Keys().ContainsOnly("status", "title", "detail")
		obj.Value("status").Number().Equal(201)
		obj.Value("title").String().Equal("TourItem successfully created")
	}
}

func TestTourItemDelete(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock TourItems db lookup
	s.TourItemService.FindTourItemByIDFn = func(ctx context.Context, id int) (*sextant.TourItem, error) {
		return &sextant.TourItem{ID: id}, nil
	}

	// Mock TourItems db lookup
	s.TourItemService.DeleteTourItemFn = func(ctx context.Context, id int) error {
		return nil
	}

	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "DELETE", "/api/touritems/99", nil))
	defer resp.Body.Close()

	if err != nil {
		t.Fatal(err)
	} else if got, want := resp.StatusCode, http.StatusNoContent; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}
}

func TestTourItemPatch(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock TourItems db lookup
	s.TourItemService.FindTourItemByIDFn = func(ctx context.Context, id int) (*sextant.TourItem, error) {
		return &sextant.TourItem{ID: id}, nil
	}

	// Mock TourItems db lookup
	s.TourItemService.UpdateTourItemFn = func(ctx context.Context, id int, upd sextant.TourItemUpdate) (*sextant.TourItem, error) {
		return &sextant.TourItem{ID: id, State: *upd.State}, nil
	}

	body := strings.NewReader("{\"state\":\"a-new-state\"}")
	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "PATCH", "/api/touritems/99", body))
	defer resp.Body.Close()

	if err != nil {
		t.Fatal(err)
	} else if got, want := resp.StatusCode, http.StatusOK; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}
}
