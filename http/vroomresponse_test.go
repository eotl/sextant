package http_test

import (
	"net/http"
	"testing"

	"eotl.supply/sextant"
	"github.com/gavv/httpexpect/v2"
)

func TestVroomResponse(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Mock VroomService.CreateRequest()
	s.VroomService.CreateRequestFn = func(tour sextant.Tour) *sextant.VroomRequest {
		return &sextant.VroomRequest{}
	}
	// Mock VroomService.Query()
	s.VroomService.QueryFn = func(*sextant.VroomRequest) (*sextant.VroomResponse, error) {
		return &sextant.VroomResponse{}, nil
	}
	// Mock GeojsonService.CreateFeatureCollection()
	s.GeojsonService.CreateFeatureCollectionFn = func(data interface{}) *sextant.GeojsonFeatureCollection {
		return &sextant.GeojsonFeatureCollection{}
	}

	e := httpexpect.New(t, s.URL())

	t.Run("CreateVroomResponse", func(t *testing.T) {
		input := "testdata/input/tour.json"
		want := "testdata/want/vroomresponse.json"

		req := e.POST("/api/vroom/solver")
		req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
		if input, err := readFile(input); err != nil {
			t.Fatal(err)
		} else if want, err := readFile(want); err != nil {
			t.Fatal(err)
		} else {
			req.WithBytes([]byte(input))
			resp := req.Expect()
			resp.Status(http.StatusOK)
			JSONIsEqual(t, resp.Body().Raw(), want)
		}

		t.Run("WithInvalidJson", func(t *testing.T) {
			input := "{invalid_json}"
			want := "testdata/want/invalid_json_error.json"
			if want, err := readFile(want); err != nil {
				t.Fatal(err)
			} else {
				req := e.POST("/api/vroom/solver")
				req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
				req.WithBytes([]byte(input))
				resp := req.Expect()
				resp.Status(http.StatusBadRequest)
				JSONIsEqual(t, resp.Body().Raw(), want)
			}
		})
	})
}
