package http

import (
	//"eotl.supply/sextant"

	"github.com/kataras/iris/v12"
)

type Status struct {
	Name string `json:"name"`
}

type Info struct {
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Location    string  `json:"location"`
	Contact     string  `json:"contact"`
	Status      string  `json:"status"`
	Service     string  `json:"service"`
	Version     Version `json:"version"`
}

type Version struct {
	Repo   string `json:"repo"`
	Branch string `json:"branch"`
	Date   string `json:"date"`
	Hash   string `json:"hash"`
}

func (s *Server) registerStatusRoutes(app *iris.Application) {
	api := app.Party("/api")
	{
		api.Use(iris.Compression)
		api.Get("/", s.handleIndexView)
		api.Get("/info", s.handleInfoView)
	}
}

func (s *Server) handleIndexView(ctx iris.Context) {
	var status Status
	status.Name = "Sextant API"

	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(status)
}

func (s *Server) handleInfoView(ctx iris.Context) {
	var version Version
	version.Repo = "https://codeberg.org/eotl/sextant"
	version.Branch = "main"
	version.Date = "2024-07-01"
	version.Hash = "xxx"

	var info Info
	info.Name = "Sextant"
	info.Description = "Sextant is a geospatial mapping application for small-scale delivery logistics"
	info.Location = "The Internet, Earth"
	info.Contact = "chat@eotl.supply"
	info.Status = "healthy"
	info.Service = "sextant"
	info.Version = version

	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(info)
}
