package http

import (
	"strconv"

	"eotl.supply/sextant"
	"github.com/kataras/iris/v12"
)

func (s *Server) registerContactAddressRoutes(app *iris.Application) {
	api := app.Party("/api/contacts")
	{
		api.Use(iris.Compression)
		api.Get("/", s.handleContactAddressIndex)
		api.Post("/", s.handleContactAddressCreate)
	}
}

func (s *Server) handleContactAddressIndex(ctx iris.Context) {
	var group *sextant.Group
	var zone *sextant.Zone
	var tours []sextant.Tour
	var err error
	// Parse url parameters
	groupIdOrSlug := ctx.URLParam("group")
	fuzzySearch := ctx.URLParam("q")
	zoneAbbr := ctx.URLParam("zone")
	contactID := ctx.URLParam("contact_id")

	if groupIdOrSlug != "" {
		group, err = s.GroupService.FindGroupByIDorSlug(ctx, groupIdOrSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find group").DetailErr(err))
			return
		}
	}

	if zoneAbbr != "" {
		zone, err = s.ZoneService.FindZoneByAbbr(ctx, zoneAbbr)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find zone").DetailErr(err))
			return
		}
	}

	if contactID != "" {
		var contactIDint int
		if contactIDint, err = strconv.Atoi(contactID); err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Failed to parse contact_id param as integer").DetailErr(err))
			return
		}
		if _, err = s.ContactService.FindContactByID(ctx, contactIDint); err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find contact_id").DetailErr(err))
			return
		}

		filter := sextant.TourFilter{ContactID: &contactIDint}
		tours, err = s.TourService.FindTours(ctx, filter)
	}

	filter := sextant.ContactAddressFilter{}
	if group != nil {
		filter.GroupID = &group.ID
	}
	if zone != nil {
		filter.ZoneID = &zone.ID
	}
	if fuzzySearch != "" {
		filter.ContactName = &fuzzySearch
		filter.AddressLabel = &fuzzySearch
	}
	if len(tours) > 0 {
		var tourIDs []int
		for _, tour := range tours {
			tourIDs = append(tourIDs, tour.ID)
		}
		filter.TourIDs = tourIDs
	}
	// Find ContactAddresses
	contactAddresses, err := s.ContactAddressService.FindContactAddresses(ctx, filter)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Failed to find contact addresses").DetailErr(err))
		return
	}
	// Prepare Response
	resp := struct {
		Contacts []sextant.ContactAddress `json:"contacts"`
	}{Contacts: contactAddresses}
	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleContactAddressCreate(ctx iris.Context) {
	body := struct {
		GroupName        string                   `json:"group"`
		ContactAddresses []sextant.ContactAddress `json:"contacts"`
	}{}
	var group *sextant.Group
	var err error

	if err = ctx.ReadJSON(&body); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to read body").DetailErr(err))
		return
	}

	if body.GroupName != "" {
		group, err = s.GroupService.FindGroupByName(ctx, body.GroupName)
		if err != nil {
			ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Unable to find group").DetailErr(err))
			return
		}
	}

	for _, contactAddress := range body.ContactAddresses {
		if contactAddress.Location, err = s.LocationService.FetchLocation(contactAddress.Label); err != nil {
			ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Failed to fetch location").DetailErr(err))
			return
		}
		if err = s.ContactAddressService.FindOrCreateContactAddress(ctx, &contactAddress); err != nil {
			ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to persist ContactAddress").DetailErr(err))
			return
		}
		if group != nil {
			groupMembership := &sextant.GroupMembership{GroupID: group.ID, ContactAddressID: contactAddress.ID}
			if err := s.GroupMembershipService.FindOrCreateGroupMembership(ctx, groupMembership); err != nil {
				ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to persist GroupMembership").DetailErr(err))
				return
			}
		}
	}
	resp := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
	}{201, "Contacts added successfully"}

	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleContactAddressView(ctx iris.Context) {
	var id int
	var err error
	caIDstr := ctx.Params().Get("user_id")
	if id, err = strconv.Atoi(caIDstr); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find user").DetailErr(err))
		return
	}
	ca, err := s.ContactAddressService.FindContactAddressByID(ctx, id)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find user").DetailErr(err))
		return
	}
	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(ca); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
