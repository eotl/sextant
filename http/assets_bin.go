//go:build bundle
// +build bundle

package http

// HandleAssets loads theme/ui files from the binary bundle.
func HandleAssets(s *Server) error {
	s.app.HandleDir("/", AssetFile())
	return nil
}
