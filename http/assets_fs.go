//go:build !bundle
// +build !bundle

package http

import (
	"fmt"
	"os"
	"path/filepath"

	"eotl.supply/sextant"
)

// HandleAssets loads theme/ui files from the file system.
func HandleAssets(s *Server) error {
	assetsPath, err := filepath.Abs(fmt.Sprintf("%s/www/dist", sextant.Root))
	if err != nil {
		return fmt.Errorf("unable to read absolute path: %s", err)
	}

	if _, err := os.Stat(assetsPath); err != nil {
		if os.IsNotExist(err) {
			return fmt.Errorf("unable to find assets directory path: %s", err)
		}
	}

	s.app.Logger().Printf("loading assets from %s", assetsPath)

	s.app.HandleDir("/", assetsPath)

	return nil
}
