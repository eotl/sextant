package http_test

import (
	"net/http"
	"testing"

	"github.com/gavv/httpexpect/v2"
)

func TestHTTPBasicAuth(t *testing.T) {
	s := MustOpenServer(t, WithBasicAuthMod("foo", "bar"))
	defer MustCloseServer(t, s)

	e := httpexpect.New(t, s.URL())

	t.Run("BasicAuthCorrectCredentials", func(t *testing.T) {
		req := e.GET("/").WithBasicAuth("foo", "bar")
		resp := req.Expect()
		resp.Status(http.StatusOK)
	})

	t.Run("BasicAuthIncorrectCredentials", func(t *testing.T) {
		req := e.GET("/").WithBasicAuth("wrong", "credentials")
		resp := req.Expect()
		resp.Status(http.StatusUnauthorized)
	})
}
