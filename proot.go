package sextant

import (
	"path/filepath"
	"runtime"
)

var (
	_, b, _, _ = runtime.Caller(0)

	// Root is the root of the source repository.
	Root = filepath.Dir(b)
)
