Sextant
=======

[![status-badge](https://ci.codeberg.org/api/badges/13905/status.svg)](https://ci.codeberg.org/repos/13905)

*Sextant* is a web application and CLI which is a collection of geo locational
services including Place locating, Tour Building, and Delivery management
functions. It is made for independent couriers, collectives, and small business
who do their own logistics fulfilment. *Sextant* consists of the command line
tool `sextant` as well as a HTTP server daemon `sextantd` and an intuitive Web
UI.

## Use Cases

The following are the primary use-cases for delivery logistics which led to the current codebase, API, and user interface of *Sextant.* The  core functionality centers around creating *Tours* which consist of multiple points on a map, have start and end points, and offer some sense of "delivery state" for each map point or delivery stop. The following personas are modeled after who may find *Sextant* useful to their operational needs.

### Courier Collectives

As *Kollektiv X* who operates as non-hiearchical bicycle delivery group (all users have all access to the system), we need to be able to create weekly *Tours* of our deliveries for our clients. Our clients range from small *Independent Producers* to *Small Stores* to *Single Delivery* tours at varying degrees of immediateness. We need a tool which makes it easy to keep track of our daily deliveries and weekly recurring tours so we can properly staff our riders to meet our clients needs. We need software which automatedly calculates delivery fees based on number of stops, volume and distance between stops.


### Independent Producer

As *Jazzy Juice Maker* I produce fresh juices for individuals and small businesses like cafe's and co-working spaces. I have recuring client list of about 40 customers. On any given week I need to create *Tours* for my juices to be delivered to my customers. These tours consist of a subset of my 40 customers, they happen on the same two days each week and are usually 8 to 15 delivery stops. I need a tool which makes this easy for a courier collective to plan their daily routes of availability and mark each Stop as it is fulfilled or unfullfilled.


### Small Store

As an *Organic Market* we have 2 weekly recurring deliveries and occassionaly get additional random deliveries requests for parties and events. We can manage this without a fancy tool, but it would be nice if this auto-generated invoices in some way. We also are looking to take on more logistics which are clients currently charge for such as picking up weekly bread deliveries and other fresh perishable items produced by small *Independent Producers*


### Single Deliveries

As a busy person in a city, I often need help delivering things around my city. I want to pay a small fee to have the item collected from my location of choice instead of me having to take it to a post office or collection point. I also want to be able to pay a higher fee to prioritize delivery.


## Features

The things which *Sextant* will be able to do once we finish [this milestone](https://codeberg.org/eotl/sextant/milestone/1594) are as follows:

- Fetch geo locations for a single address or list in a CSV
- Import CSV of deliveries and optional pickup and delivery locations and metadata (phone, email)
- Intelligently solves a "tour" finding optimal routes using the [Vroom engine](https://github.com/VROOM-Project/vroom)
- Converts lists of data to geojson format
- Create groups of multiple contacts / locations
- Transcoding of geo coordinates to [Where39](https://github.com/arcbtc/where39) words

If you think *Sextant* might be a good fit for some of your needs, but it is missing a feature, feel free to [open an issue](https://codeberg.org/eotl/sextant/issues/new) or [contact us](https://eotl.supply/contact)


## Documentation

- [Architecture](docs/architecture.md)
- [Authentication](docs/auth.md)
- [CLI](docs/cli.md)
- [Contributing](docs/contributing.md)
- [Deploy](docs/deployment.md)
- [Notes](docs/notes.md)
- [Release](docs/release.md)
- [REST API](docs/rest-api.md)
