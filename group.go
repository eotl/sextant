package sextant

import (
	"context"
	"time"
)

type Group struct {
	ID        int       `yaml:"-" json:"id"`
	Name      string    `json:"name"`
	Slug      string    `yaml:"-" json:"slug"`
	Tours     []Tour    `yaml:"-" json:"-"`
	CreatedAt time.Time `yaml:"-" json:"-"`
	UpdatedAt time.Time `yaml:"-" json:"-"`
}

type GroupFilter struct {
	ID     *int    `json:"id"`
	Name   *string `json:"name"`
	Slug   *string `json:"slug"`
	Offset int     `json:"offset"`
	Limit  int     `json:"limit"`
	TourID *int    `json:"tour_id"`
}

func (g *Group) Validate() error {
	return nil
}

type GroupService interface {
	CreateGroup(ctx context.Context, group *Group) error

	DeleteGroup(ctx context.Context, id int) error

	FindAllGroups(ctx context.Context) ([]Group, error)

	FindGroupByID(ctx context.Context, id int) (*Group, error)

	FindGroupByName(ctx context.Context, name string) (*Group, error)

	FindGroupByIDorSlug(ctx context.Context, idOrSlug string) (*Group, error)

	FindGroupBySlug(ctx context.Context, slug string) (*Group, error)
}
