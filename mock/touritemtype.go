package mock

import (
	"context"
	"eotl.supply/sextant"
)

type TourItemtypeService struct {
	FindTourItemtypeByNameFn func(ctx context.Context, name string) (*sextant.TourItemtype, error)
}

func (s *TourItemtypeService) FindTourItemtypeByName(ctx context.Context, name string) (*sextant.TourItemtype, error) {
	return s.FindTourItemtypeByNameFn(ctx, name)
}
