package mock

import (
	"eotl.supply/sextant"
	"io"
	"net/http"
	"net/http/httptest"
)

var _ sextant.HttpClient = (*HttpClient)(nil)

// Please call Server.Close() when finished
type HttpClient struct {
	client *http.Client
	url    string
}

func NewHttpClient(statusCode int, data interface{}) (*HttpClient, *httptest.Server) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.WriteHeader(statusCode)
		rw.Write([]byte(data.(string)))
	}))
	return &HttpClient{server.Client(), server.URL}, server
}

func (c *HttpClient) Get(path string) (*http.Response, error) {
	url := c.url + "/" + path
	return c.client.Get(url)
}

func (c *HttpClient) Post(url, contentType string, body io.Reader) (*http.Response, error) {
	// Overwrites url parameter
	url = c.url
	return c.client.Post(url, contentType, body)
}
