package mock

import (
	"context"
	"eotl.supply/sextant"
)

type LocationService struct {
	FetchLocationFn        func(address string) (*sextant.Location, error)
	FindOrCreateLocationFn func(ctx context.Context, locatoon *sextant.Location) error
	FindLocationByIDFn     func(ctx context.Context, id int) (*sextant.Location, error)
	UpdateLocationsFn      func(people ...[]sextant.ContactAddress) []sextant.ContactAddress
}

func (s *LocationService) FetchLocation(address string) (*sextant.Location, error) {
	return s.FetchLocationFn(address)
}

func (s *LocationService) FindOrCreateLocation(ctx context.Context, location *sextant.Location) error {
	return s.FindOrCreateLocationFn(ctx, location)
}

func (s *LocationService) FindLocationByID(ctx context.Context, id int) (*sextant.Location, error) {
	return s.FindLocationByIDFn(ctx, id)
}

func (s *LocationService) UpdateLocations(people ...[]sextant.ContactAddress) []sextant.ContactAddress {
	return s.UpdateLocationsFn(people...)
}
