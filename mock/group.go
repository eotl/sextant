package mock

import (
	"context"
	"eotl.supply/sextant"
)

type GroupService struct {
	CreateGroupFn         func(ctx context.Context, group *sextant.Group) error
	DeleteGroupFn         func(ctx context.Context, id int) error
	FindAllGroupsFn       func(ctx context.Context) ([]sextant.Group, error)
	FindGroupByIDFn       func(ctx context.Context, id int) (*sextant.Group, error)
	FindGroupByIDorSlugFn func(ctx context.Context, idOrSlug string) (*sextant.Group, error)
	FindGroupByNameFn     func(ctx context.Context, name string) (*sextant.Group, error)
	FindGroupBySlugFn     func(ctx context.Context, slug string) (*sextant.Group, error)
}

func (s *GroupService) CreateGroup(ctx context.Context, group *sextant.Group) error {
	return s.CreateGroupFn(ctx, group)
}

func (s *GroupService) DeleteGroup(ctx context.Context, id int) error {
	return s.DeleteGroupFn(ctx, id)
}

func (s *GroupService) FindAllGroups(ctx context.Context) ([]sextant.Group, error) {
	return s.FindAllGroupsFn(ctx)
}

func (s *GroupService) FindGroupByID(ctx context.Context, id int) (*sextant.Group, error) {
	return s.FindGroupByIDFn(ctx, id)
}

func (s *GroupService) FindGroupByIDorSlug(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
	return s.FindGroupByIDorSlugFn(ctx, idOrSlug)
}

func (s *GroupService) FindGroupByName(ctx context.Context, name string) (*sextant.Group, error) {
	return s.FindGroupByNameFn(ctx, name)
}

func (s *GroupService) FindGroupBySlug(ctx context.Context, slug string) (*sextant.Group, error) {
	return s.FindGroupBySlugFn(ctx, slug)
}
