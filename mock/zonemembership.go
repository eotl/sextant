package mock

import (
	"context"
	"eotl.supply/sextant"
)

type ZoneMembershipService struct {
	FindZoneMembershipByIDsFn    func(ctx context.Context, zoneID int, locationID int) (*sextant.ZoneMembership, error)
	FindOrCreateZoneMembershipFn func(ctx context.Context, m *sextant.ZoneMembership) error
}

func (s *ZoneMembershipService) FindZoneMembershipByIDs(ctx context.Context, zoneID int, locationID int) (*sextant.ZoneMembership, error) {
	return s.FindZoneMembershipByIDsFn(ctx, zoneID, locationID)
}

func (s *ZoneMembershipService) FindOrCreateZoneMembership(ctx context.Context, zm *sextant.ZoneMembership) error {
	return s.FindOrCreateZoneMembershipFn(ctx, zm)
}
