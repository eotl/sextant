package mock

import (
	"eotl.supply/sextant"
)

type GeojsonService struct {
	CreateFeatureFn           func(data interface{}) *sextant.GeojsonFeature
	CreateFeatureCollectionFn func(data interface{}) *sextant.GeojsonFeatureCollection
}

func (s *GeojsonService) CreateFeature(data interface{}) *sextant.GeojsonFeature {
	return s.CreateFeatureFn(data)
}

func (s *GeojsonService) CreateFeatureCollection(data interface{}) *sextant.GeojsonFeatureCollection {
	return s.CreateFeatureCollectionFn(data)
}
