package mock

import (
	"context"
	"eotl.supply/sextant"
)

type ZoneService struct {
	CreateZoneFn     func(ctx context.Context, zone *sextant.Zone) error
	DeleteZoneFn     func(ctx context.Context, id int) error
	FindZoneByAbbrFn func(ctx context.Context, abbr string) (*sextant.Zone, error)
	FindZoneByIDFn   func(ctx context.Context, id int) (*sextant.Zone, error)
	FindZonesFn      func(ctx context.Context, filter sextant.ZoneFilter) ([]*sextant.Zone, int, error)
	SetZoneAbbrFn    func(zones []sextant.Zone) ([]sextant.Zone, error)
}

func (s *ZoneService) CreateZone(ctx context.Context, zone *sextant.Zone) error {
	return s.CreateZoneFn(ctx, zone)
}

func (s *ZoneService) DeleteZone(ctx context.Context, id int) error {
	return s.DeleteZoneFn(ctx, id)
}

func (s *ZoneService) FindZoneByAbbr(ctx context.Context, abbr string) (*sextant.Zone, error) {
	return s.FindZoneByAbbrFn(ctx, abbr)
}

func (s *ZoneService) FindZoneByID(ctx context.Context, id int) (*sextant.Zone, error) {
	return s.FindZoneByIDFn(ctx, id)
}

func (s *ZoneService) FindZones(ctx context.Context, filter sextant.ZoneFilter) ([]*sextant.Zone, int, error) {
	return s.FindZonesFn(ctx, filter)
}

func (s *ZoneService) SetZoneAbbr(zones []sextant.Zone) ([]sextant.Zone, error) {
	return s.SetZoneAbbrFn(zones)
}
