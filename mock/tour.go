package mock

import (
	"context"
	"eotl.supply/sextant"
)

type TourService struct {
	CreateTourFn         func(ctx context.Context, tour *sextant.Tour) error
	DeleteTourFn         func(ctx context.Context, id int) error
	FindToursFn          func(ctx context.Context, filter sextant.TourFilter) ([]sextant.Tour, error)
	FindTourByIDorSlugFn func(ctx context.Context, idOrSlug string) (*sextant.Tour, error)
}

func (s *TourService) CreateTour(ctx context.Context, tour *sextant.Tour) error {
	return s.CreateTourFn(ctx, tour)
}

func (s *TourService) DeleteTour(ctx context.Context, id int) error {
	return s.DeleteTourFn(ctx, id)
}

func (s *TourService) FindTours(ctx context.Context, filter sextant.TourFilter) ([]sextant.Tour, error) {
	return s.FindToursFn(ctx, filter)
}

func (s *TourService) FindTourByIDorSlug(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
	return s.FindTourByIDorSlugFn(ctx, idOrSlug)
}
