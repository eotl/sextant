package sextant

import (
	"context"
	"errors"
	"time"
)

type Location struct {
	ID        int       `json:"-" yaml:"-"`
	Lon       float64   `json:"lon,omitempty"`
	Lat       float64   `json:"lat,omitempty"`
	Words     string    `json:"words,omitempty"`
	CreatedAt time.Time `json:"-" yaml:"-"`
	UpdatedAt time.Time `json:"-" yaml:"-"`
}

func (l *Location) ToArray() *[2]float64 {
	// Assumes coordinates [0,0] are never used:
	if l.Lon == 0 || l.Lat == 0 {
		return nil
	}
	out := [2]float64{l.Lon, l.Lat}
	return &out
}

func (l *Location) Validate() error {
	if l.Lat == 0 || l.Lon == 0 {
		return errors.New("Location Lat and Lon required.")
	}
	return nil
}

func (l *Location) IsGeoEqual(o *Location) bool {
	return (l.Lat == o.Lat && l.Lon == o.Lon)
}

type LocationDecode struct {
	ID        int     `json:"-"`
	Lon       float64 `json:",string"`
	Lat       float64 `json:",string"`
	Words     string
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}

type LocationFilter struct {
	ID     *int     `json:"id"`
	Lat    *float64 `json:"lat"`
	Lon    *float64 `json:"lon"`
	Offset int      `json:"offset"`
	Limit  int      `json:"limit"`
	// External
	ZoneID *int `json:"-"`
}

type LocationService interface {
	FetchLocation(address string) (*Location, error)

	UpdateLocations(people ...[]ContactAddress) (failed []ContactAddress)

	FindOrCreateLocation(ctx context.Context, location *Location) error

	FindLocationByID(ctx context.Context, id int) (*Location, error)
}

type LocationWordService interface {
	ToWords(location *Location) (string, error)

	FromWords(words string) (*Location, error)
}
