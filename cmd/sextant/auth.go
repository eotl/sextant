package main

import (
	"flag"
	"fmt"
	"strings"
)

// AuthCommand represents a collection of auth related subcommands.
type AuthCommand struct{}

// Run executes the command which delegates to other subcommands.
func (c *AuthCommand) Run(args []string) error {
	// Shift off the subcommand name, if available.
	var cmd string
	if len(args) > 0 && !strings.HasPrefix(args[0], "-") {
		cmd, args = args[0], args[1:]
	}

	// Delegete to the appropriate subcommand.
	switch cmd {
	case "grant":
		return (NewAuthGrantCommand()).Run(args)
	case "revoke":
		return (NewAuthRevokeCommand()).Run(args)
	case "", "-h", "help":
		c.usage()
		return flag.ErrHelp
	default:
		return fmt.Errorf("sextant auth %s: unknown command", cmd)
	}
}

// usage prints the subcommand usage to STDOUT.
func (c *AuthCommand) usage() {
	fmt.Println(`
Manage access to sextantd

Usage:

	sextant auth <command> [arguments]

The commands are:

	grant       
	revoke
`[1:])
}
