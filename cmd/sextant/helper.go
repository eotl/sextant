package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"io/ioutil"
	"os"
)

func MarshalIndent(data interface{}) ([]byte, error) {
	return json.MarshalIndent(data, "", "  ")
}

// Reads and returns command input from Stdin
// or a file - the first argument after flags
func GetInputData(fs *flag.FlagSet) ([]byte, error) {
	var data []byte
	var err error

	if len(fs.Args()) < 1 || fs.Args()[0] == "-" {
		stat, _ := os.Stdin.Stat()
		if (stat.Mode() & os.ModeCharDevice) == 0 {
			scanner := bufio.NewScanner(os.Stdin)
			for scanner.Scan() {
				data = append(data, scanner.Bytes()...)
			}
			if err = scanner.Err(); err != nil {
				return nil, err
			}
		} else {
			return nil, errors.New("Missing required file")
		}
	} else {
		inputFile := fs.Args()[0]
		data, err = ioutil.ReadFile(inputFile)
		if err != nil {
			return nil, err
		}
	}
	return data, nil
}
