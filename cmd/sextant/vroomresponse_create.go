package main

import (
	"eotl.supply/sextant"
	"eotl.supply/sextant/inmem"
	"flag"
	"fmt"
)

type VroomResponseCreateCommand struct {
	ConfigPath string
}

func (c *VroomResponseCreateCommand) Run(args []string) error {
	fs := flag.NewFlagSet("sextant-solve", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	url := fs.String("u", config.VroomUrl, "Vroom solver url")
	writeRequest := fs.Bool("w", false, "write sextant.VroomRequest to vroom_request.json")
	showHelp := fs.Bool("h", false, "show help")

	if err := fs.Parse(args); err != nil {
		return err
	}

	if *showHelp {
		c.usage(config)
		return nil
	}

	// Decode input
	inputData, err := GetInputData(fs)
	if err != nil {
		return err
	}

	encoder := inmem.JsonEncoder{}
	sextantStruct, err := encoder.DecodeFromJson(inputData)
	if err != nil {
		return err
	}
	tour := sextantStruct.(sextant.Tour)

	vroomService := inmem.NewVroomService(nil, *url, true)
	vroomRequest := vroomService.CreateRequest(tour)
	if *writeRequest {
		fileService := inmem.NewFileService(true)
		_, err := fileService.ToJSON("vroom_request.json", vroomRequest, ".")
		if err != nil {
			return err
		}
	}
	vroomResponse, err := vroomService.Query(vroomRequest)
	if err != nil {
		return err
	}

	output, err := encoder.EncodeToJson(*vroomResponse)
	if err != nil {
		return err
	}

	fmt.Println(string(output))
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *VroomResponseCreateCommand) usage(config Config) {
	fmt.Println(`
Solves a tour finding optimal routes

Usage:

	sextant solve [-hw] [-c config_file] [-u url] file 

Arguments:

	The file should be a sextant.Tour encoded with sextant.inmem.JsonEncoder.
	If file is "-" or absent the command reads from the standard input.

	-c config_file
		Path to config file (default: ` + DefaultConfigPath + `)
	-u url
		Specifies url to Vroom solver (default: ` + config.VroomUrl + `\)
	-w
		write sextant.VroomRequest to vroom_request.json

`[1:])
}
