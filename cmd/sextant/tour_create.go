package main

import (
	"eotl.supply/sextant"
	"eotl.supply/sextant/csv"
	"eotl.supply/sextant/inmem"
	"eotl.supply/sextant/yaml"
	"errors"
	"flag"
	"fmt"
	"os"
)

type TourCreateCommand struct {
}

// Reads contacts from file input (csv or yaml)
func (c *TourCreateCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-import", flag.ContinueOnError)
	deliveriesFile := fs.String("d", "", "A csv file of perons which describe the deliveries. Required.")
	var csvHeader string
	fs.StringVar(&csvHeader, "H", "", "String to replace deliveries csv file header with")
	fs.StringVar(&csvHeader, "csv-header", "", "String to replace deliveries csv file header with")
	var csvPrepend bool
	fs.BoolVar(&csvPrepend, "P", false, "Prepend deliveries csv file header")
	fs.BoolVar(&csvPrepend, "csv-prepend", false, "Prepend deliveries csv file header")
	pickupsFile := fs.String("p", "", "A yaml file of contacts which describe the pickups")
	ridersFile := fs.String("r", "", "A yaml file of contacts that describe the riders")
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}
	if *deliveriesFile == "" {
		return errors.New("Missing required file")
	}
	// Update deliveries CSV Header
	if csvHeader != "" {
		fileService := inmem.NewFileService(false)
		action := "replace"
		if csvPrepend {
			action = "prepend"
		}
		filepath, err := fileService.SetFirstLine(*deliveriesFile, csvHeader, action)
		if err != nil {
			return err
		}
		defer os.Remove(filepath)
		deliveriesFile = &filepath
	}
	// Import
	csvDecoder := csv.NewContactAddressDecoder(true)
	deliveries, err := csvDecoder.DecodeContactAddresses(*deliveriesFile)
	if err != nil {
		return err
	}
	yamlDecoder := yaml.NewContactAddressDecoder(true)
	pickups := []sextant.ContactAddress{}
	if *pickupsFile != "" {
		pickups, err = yamlDecoder.DecodeContactAddresses(*pickupsFile)
		if err != nil {
			return err
		}
	}
	riders := []sextant.ContactAddress{}
	if *ridersFile != "" {
		riders, err = yamlDecoder.DecodeContactAddresses(*ridersFile)
		if err != nil {
			return err
		}
	}

	tour := sextant.Tour{}
	for i, _ := range deliveries {
		item := sextant.TourItem{ContactAddress: &deliveries[i]}
		tour.Deliveries = append(tour.Deliveries, item)
	}
	for i, _ := range pickups {
		item := sextant.TourItem{ContactAddress: &pickups[i]}
		tour.Pickups = append(tour.Pickups, item)
	}
	for i, _ := range riders {
		item := sextant.TourItem{ContactAddress: &riders[i]}
		tour.Riders = append(tour.Riders, item)
	}

	jsonEncoder := inmem.JsonEncoder{}
	output, err := jsonEncoder.EncodeToJson(tour)
	if err != nil {
		return err
	}

	fmt.Println(string(output))
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *TourCreateCommand) usage() {
	fmt.Println(`
Imports deliveries and optionally pickups and riders.
Returns a sextant.Tour as json.

Usage:

	sextant tour [-h] [-H header_string [-P]] [-p pickups_file] [-r riders_file] -d deliveries_file

Arguments:

	-d deliveries_file
		Load deliveries from csv file
	-H header_string
	--csv-header header_string
		The string replaces first line in deliveries csv file
	-P 
	--csv-prepend
		Given '-H string' is set, enabling this flag will prepend 
		the string to deliveries csv file
	-p pickups_file
		Load pickups from yaml file
	-r riders_file
		Load riders from yaml file

	For a detailed specification of input files
	please see README document.

`[1:])
}
