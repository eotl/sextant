package main

import (
	"context"
	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
	"eotl.supply/sextant/txt"
	"errors"
	"flag"
	"fmt"
	"regexp"
)

type AuthGrantCommand struct {
	ConfigPath string
	DB         *sqlite.DB
}

func (c *AuthGrantCommand) Run(args []string) (err error) {
	var group *sextant.Group
	var contact *sextant.Contact
	fs := flag.NewFlagSet("sextant-auth-grant", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	role := fs.String("role", "", "Valid value: (admin|organizer|participant). Required.")
	groupIDorSlug := fs.String("group", "", "Group ID or slug. Required if role is organizer.")
	contactID := fs.Int("contact", -1, "The contact ID. Required if role is participant.")
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}
	if match, _ := regexp.Match(`admin|organizer|participant|user`, []byte(*role)); !match {
		return errors.New("invalid value of role parameter")
	}
	if *role == "organizer" && len(*groupIDorSlug) < 1 {
		return errors.New("group parameter invalid or missing")
	}
	if *role == "participant" && *contactID < 1 {
		return errors.New("contact parameter invalid or missing")
	}
	if len(fs.Args()) == 0 {
		return errors.New("Missing required key.")
	}
	key := fs.Args()[0]

	// Process group and contact parameters
	if *role == "organizer" || *role == "participant" {
		// Open database
		if c.DB.DSN, err = expandDSN(config.DSN); err != nil {
			return fmt.Errorf("cannot expand dsn: %w", err)
		}
		if err := c.DB.Open(); err != nil {
			return fmt.Errorf("cannot open db: %w", err)
		}
		defer c.DB.Close()
		ctx := context.Background()
		if len(*groupIDorSlug) > 0 {
			svc := sqlite.NewGroupService(c.DB)
			if group, err = svc.FindGroupByIDorSlug(ctx, *groupIDorSlug); err != nil {
				return err
			}
		}
		if *contactID > 0 {
			svc := sqlite.NewContactService(c.DB)
			if contact, err = svc.FindContactByID(ctx, *contactID); err != nil {
				return err
			}
		}
	}

	svc := txt.NewSsrAuthService(config.SsrAuthFilepath, true)
	userRules := sextant.SsrAuthRuleset{}
	userRules.Add("/api/geocode/address", "rw")
	userRules.Add("/api/geocode/addresses", "rw")
	userRules.Add("/api/vroom", "rw")
	userRules.Add("/api/vroom/solver", "rw")
	userRules.Add("/api/zones", "r")

	if *role == "user" {
		if err := svc.WriteRules(key, userRules); err != nil {
			return err
		}
		return nil
	}

	// Write rules
	if *role == "organizer" && group != nil {
		groupRules := sextant.SsrAuthRuleset{}
		groupRules.Rules = userRules.Rules
		groupRules.Add(fmt.Sprintf("/api/groups/%d", group.ID), "rw")
		groupRules.Add(fmt.Sprintf("/api/groups/%d/*", group.ID), "rw")
		groupRules.Add(fmt.Sprintf("/api/groups/%s", group.Slug), "rw")
		groupRules.Add(fmt.Sprintf("/api/groups/%s/*", group.Slug), "rw")
		if err := svc.WriteRules(key, groupRules); err != nil {
			return err
		}
		return nil
	}
	if *role == "participant" && contact != nil {
		participantRules := sextant.SsrAuthRuleset{}
		participantRules.Rules = userRules.Rules
		participantRules.Add(fmt.Sprintf("/api/users/%d", contact.ID), "rw")
		participantRules.Add(fmt.Sprintf("/api/users/%d/*", contact.ID), "rw")
		if err := svc.WriteRules(key, participantRules); err != nil {
			return err
		}
		return nil
	}
	if *role == "admin" {
		adminRules := sextant.SsrAuthRuleset{}
		adminRules.Rules = userRules.Rules
		adminRules.Add("/api/contacts", "rw")
		adminRules.Add("/api/contacts/*", "rw")
		adminRules.Add("/api/groups", "rw")
		adminRules.Add("/api/groups/*", "rw")
		adminRules.Add("/api/tours", "rw")
		adminRules.Add("/api/tours/*", "rw")
		adminRules.Add("/api/touritems", "rw")
		adminRules.Add("/api/touritems/*", "rw")
		adminRules.Add("/api/users", "rw")
		adminRules.Add("/api/users/*", "rw")
		if err := svc.WriteRules(key, adminRules); err != nil {
			return err
		}
		return nil
	}
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *AuthGrantCommand) usage() {
	fmt.Println(`
Grant access to sextantd via ssr auth

Adds relevant rules to ssr auth file.

Usage:

	sextant auth grant [-h] --role admin key |
	                        --role user key |
	                        --role organizer --group groupID key |
	                        --role participant --contact contactID key

Arguments:

	key
		the ssr public key
	
	--role (admin|organizer|participant|user)
		Role implies the kind of rules that are written to ssr auth. Required.
	--group groupID
		Required if role is organizer. 	
	--contact contactID 
		Required if role is participant.
`[1:])
}

func NewAuthGrantCommand() *AuthGrantCommand {
	return &AuthGrantCommand{
		DB: sqlite.NewDB(""),
	}
}
