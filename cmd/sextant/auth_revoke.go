package main

import (
	"eotl.supply/sextant/txt"
	"errors"
	"flag"
	"fmt"
)

type AuthRevokeCommand struct {
	ConfigPath string
}

// Reads contacts from file input (csv or yaml)
func (c *AuthRevokeCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-auth-revoke", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}
	if len(fs.Args()) == 0 {
		return errors.New("Missing required key.")
	}
	key := fs.Args()[0]

	svc := txt.NewSsrAuthService(config.SsrAuthFilepath, true)
	if err := svc.RemoveRules(key); err != nil {
		return err
	}

	return nil
}

// usage print usage information for the command to STDOUT.
func (c *AuthRevokeCommand) usage() {
	fmt.Println(`
Revoke access to sextantd via ssr auth

Removes all rules (lines) from ssr auth file that begin with key.

Usage:

	sextant auth revoke [-h] key

Arguments:

	key
		the ssr public key of the user 

`[1:])
}

func NewAuthRevokeCommand() *AuthRevokeCommand {
	return &AuthRevokeCommand{}
}
