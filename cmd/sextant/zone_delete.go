package main

import (
	"context"
	"eotl.supply/sextant/sqlite"
	"errors"
	"flag"
	"fmt"
)

type ZoneDeleteCommand struct {
	ConfigPath string
	DB         *sqlite.DB
}

// Reads contacts from file input (csv or yaml)
func (c *ZoneDeleteCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-zone-list", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	id := fs.Int("id", -1, "zone id")
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}
	if *id < 0 {
		return errors.New("Arguments missing")
	}

	// Delete from db
	if c.DB.DSN, err = expandDSN(config.DSN); err != nil {
		return fmt.Errorf("cannot expand dsn: %w", err)
	}
	if err := c.DB.Open(); err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}
	defer c.DB.Close()
	ctx := context.Background()
	svc := sqlite.NewZoneService(c.DB)

	if err := svc.DeleteZone(ctx, *id); err != nil {
		return err
	}
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *ZoneDeleteCommand) usage() {
	fmt.Println(`
Delete a zone.

Usage:

	sextant zone delete [-h] -id id

Arguments:

	-id zone_id
		The id of the zone to delete. Required.
`[1:])
}

func NewZoneDeleteCommand() *ZoneDeleteCommand {
	return &ZoneDeleteCommand{
		DB: sqlite.NewDB(""),
	}
}
