package main

import (
	"flag"
	"fmt"
	"strings"
)

// ZoneCommand represents a collection of zone-related subcommands.
type ZoneCommand struct{}

// Run executes the command which delegates to other subcommands.
func (c *ZoneCommand) Run(args []string) error {
	// Shift off the subcommand name, if available.
	var cmd string
	if len(args) > 0 && !strings.HasPrefix(args[0], "-") {
		cmd, args = args[0], args[1:]
	}

	// Delegete to the appropriate subcommand.
	switch cmd {
	case "create":
		return (NewZoneCreateCommand()).Run(args)
	case "delete":
		return (NewZoneDeleteCommand()).Run(args)
	case "list":
		return (NewZoneListCommand()).Run(args)
	case "", "-h", "help":
		c.usage()
		return flag.ErrHelp
	default:
		return fmt.Errorf("sextant zone %s: unknown command", cmd)
	}
}

// usage prints the subcommand usage to STDOUT.
func (c *ZoneCommand) usage() {
	fmt.Println(`
Manage sextant zones

Usage:

	sextant zone <command> [arguments]

The commands are:

	list        all available zones
	create      a new zone
	delete      a zone by id
`[1:])
}
