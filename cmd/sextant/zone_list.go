package main

import (
	"context"
	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
	"flag"
	"fmt"
	"github.com/jedib0t/go-pretty/v6/table"
	"os"
)

type ZoneListCommand struct {
	ConfigPath string
	DB         *sqlite.DB
}

func (c *ZoneListCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-zone-list", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}

	// Persist data
	if c.DB.DSN, err = expandDSN(config.DSN); err != nil {
		return fmt.Errorf("cannot expand dsn: %w", err)
	}
	if err := c.DB.Open(); err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}
	defer c.DB.Close()
	ctx := context.Background()
	svc := sqlite.NewZoneService(c.DB)
	zones, _, err := svc.FindZones(ctx, sextant.ZoneFilter{})
	if err != nil {
		return err
	}

	if len(zones) > 0 {
		t := table.NewWriter()
		t.SetOutputMirror(os.Stdout)
		t.AppendHeader(table.Row{"ID", "PID", "Abbr", "Name"})
		tableRows := []table.Row{}
		for _, zone := range zones {
			var pid string
			if zone.ParentID != nil {
				pid = fmt.Sprintf("%d", *zone.ParentID)
			}
			tableRows = append(tableRows, table.Row{zone.ID, pid, zone.Abbr, zone.Name})
		}
		t.AppendRows(tableRows)
		t.SetStyle(table.StyleLight)
		t.Render()
	}
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *ZoneListCommand) usage() {
	fmt.Println(`
List zones.

Usage:

	sextant zone list [-h]

`[1:])
}

func NewZoneListCommand() *ZoneListCommand {
	return &ZoneListCommand{
		DB: sqlite.NewDB(""),
	}
}
