package main

import (
	"eotl.supply/sextant"
	"eotl.supply/sextant/inmem"
	"errors"
	"flag"
	"fmt"
)

type GeojsonCreateCommand struct {
}

func (c *GeojsonCreateCommand) Run(args []string) error {
	fs := flag.NewFlagSet("sextant-geojson", flag.ContinueOnError)
	showHelp := fs.Bool("h", false, "show help")

	if err := fs.Parse(args); err != nil {
		return err
	}

	if *showHelp {
		c.usage()
		return nil
	}

	inputData, err := GetInputData(fs)
	if err != nil {
		return err
	}

	jsonEncoder := inmem.JsonEncoder{}
	sextantStruct, err := jsonEncoder.DecodeFromJson(inputData)
	if err != nil {
		return err
	}

	geojsonService := inmem.NewGeojsonService()
	var geojsonStruct *sextant.GeojsonFeatureCollection
	switch t := sextantStruct.(type) {

	case []sextant.Contact:
		contacts := sextantStruct.([]sextant.Contact)
		geojsonStruct = geojsonService.CreateFeatureCollection(contacts)

	case sextant.Tour:
		tour := sextantStruct.(sextant.Tour)
		contacts := tour.Deliveries
		geojsonStruct = geojsonService.CreateFeatureCollection(contacts)

	case sextant.VroomResponse:
		vroomResponse := sextantStruct.(sextant.VroomResponse)
		routes := vroomResponse.GetRoutes()
		geojsonStruct = geojsonService.CreateFeatureCollection(routes)

	default:
		err_msg := fmt.Sprintf("Type not support: %T", t)
		err := errors.New(err_msg)
		return err
	}

	output, err := MarshalIndent(geojsonStruct)
	if err != nil {
		return err
	}

	fmt.Println(string(output))

	return nil
}

// usage print usage information for the command to STDOUT.
func (c *GeojsonCreateCommand) usage() {
	fmt.Println(`
Converts to geojson format

Usage:

	sextant geojson [-h] file

Arguments:

	The file should be sextant.Tour or sextant.VroomResponse encoded with
	sextant.inmem.JsonEncoder. If file is "-" or absent the command reads
	from the standard input.

Output:

  For sextant.Tour: it converts tour.Delivieries to geojson
  For sextant.VroomResponse: it converts vroomResponse.GetRoutes() to geojson

`[1:])
}
