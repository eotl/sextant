package main

import (
	"context"
	"eotl.supply/sextant/sqlite"
	"flag"
	"fmt"
	"github.com/jedib0t/go-pretty/v6/table"
	"os"
)

type GroupListCommand struct {
	ConfigPath string
	DB         *sqlite.DB
}

// Reads contacts from file input (csv or yaml)
func (c *GroupListCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-group-list", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}

	// Persist data
	if c.DB.DSN, err = expandDSN(config.DSN); err != nil {
		return fmt.Errorf("cannot expand dsn: %w", err)
	}
	if err := c.DB.Open(); err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}
	defer c.DB.Close()
	ctx := context.Background()
	svc := sqlite.NewGroupService(c.DB)
	groups, err := svc.FindAllGroups(ctx)
	if err != nil {
		return err
	}
	if len(groups) > 0 {
		t := table.NewWriter()
		t.SetOutputMirror(os.Stdout)
		t.AppendHeader(table.Row{"ID", "Name", "Slug"})
		tableRows := []table.Row{}
		for _, group := range groups {
			tableRows = append(tableRows, table.Row{group.ID, group.Name, group.Slug})
		}
		t.AppendRows(tableRows)
		t.SetStyle(table.StyleLight)
		t.Render()
	}
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *GroupListCommand) usage() {
	fmt.Println(`
List groups.

Usage:

	sextant group list [-h]

`[1:])
}

func NewGroupListCommand() *GroupListCommand {
	return &GroupListCommand{
		DB: sqlite.NewDB(""),
	}
}
