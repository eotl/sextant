package sextant

import (
	"context"
	"time"
)

type TourItemtype struct {
	ID        int       `yaml:"-" json:"-"`
	Name      string    `yaml:"-" json:"-"`
	CreatedAt time.Time `yaml:"-" json:"-"`
	UpdatedAt time.Time `yaml:"-" json:"-"`
}

type TourItemtypeFilter struct {
	ID     *int    `json:"id"`
	Name   *string `json:"name"`
	Offset int     `json:"offset"`
	Limit  int     `json:"limit"`
}

func (t *TourItemtype) Validate() error {
	return nil
}

type TourItemtypeService interface {
	FindTourItemtypeByName(ctx context.Context, name string) (*TourItemtype, error)
}
