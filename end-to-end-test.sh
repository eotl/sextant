#! /usr/bin/env bash

set -e # Exit when command fails

readonly DEPS="jq base64 http"

error() { printf "Error: ${1}\n"; exit 1; }

check_deps() {
	for c in $DEPS; do
		command -v "$c" > /dev/null || error "Command not found, please install it: ${c}"
	done
}

check_deps

mkdir -p logs
#TEST_LOG_FILE=logs/end2end-$(date -u +"%Y%m%d%H%M%SZ").log
TEST_LOG_FILE=logs/end2end.log
#SERVER_LOG_FILE=logs/server-$(date -u +"%Y%m%d%H%M%SZ").log
SERVER_LOG_FILE=logs/server.log

onexit(){

  echo "Killing server" | tee -a "$TEST_LOG_FILE"
  kill "${SERVER_PID}" >> "$TEST_LOG_FILE" 2>&1
  echo "... Done" | tee -a "$TEST_LOG_FILE"

  if [[ -z "${SUCCESS}" ]]; then
    echo ""
    echo ""
    echo "#######################################"
    echo "# Client log:"
    echo "#######################################"
    echo ""

    cat "$TEST_LOG_FILE"

    echo ""
    echo ""
    echo "#######################################"
    echo "# Server log:"
    echo "#######################################"
    echo ""

    cat "$SERVER_LOG_FILE"
  fi
}
trap onexit EXIT

base64_nowrap() {
	# some base64 implementaitons do not have a "no wrap" option
	# this wrapper implements it independently
	while read i
	do
		echo "$i"
	done | base64 | tr -d '\n\r'
}

echo "Running end2end test for sextant-daemon. Logs are written to $TEST_LOG_FILE" | tee "$TEST_LOG_FILE"
mkdir -p logs

echo "Rebuilding sextant ..." | tee -a "$TEST_LOG_FILE"
make build
echo "... Done. " | tee -a "$TEST_LOG_FILE"

echo "Clearing SQLITE database file and repopulating zones ..." | tee -a "$TEST_LOG_FILE"
make rebuild-db
echo "... Done" | tee -a "$TEST_LOG_FILE"

echo "Starting server ..." | tee -a "$TEST_LOG_FILE"
./sextantd > "$SERVER_LOG_FILE" 2>&1 & SERVER_PID=${!} # Storing server pid so we can kill it after script is done
echo "... Done. Server running with PID $SERVER_PID, logging output to $SERVER_LOG_FILE"


# --- Zones ---

# | GET    | /api/zones                              | No   | All   |

# Zones Test 1 - Get zones

echo "Retrieving Zones ..." | tee -a "$TEST_LOG_FILE"
#ZONE_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/zones")
#echo "$ZONE_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "FIXME: Functionality not implemented in server.. :(" | tee -a "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Zones test 2 - Get specific zone
ZONE_ID="BRLN"
echo "Retrieving Zones by id: ${ZONE_ID} ..." | tee -a "$TEST_LOG_FILE"
ZONE_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/zones?id=${ZONE_ID}")
echo "$ZONE_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
ZONE_RESPONSE_ID=$(echo $ZONE_QUERY_RESPONSE | jq -r .features[0].properties.abbr)
echo "... Done. Zone ID reponse: ${ZONE_RESPONSE_ID}" | tee -a "$TEST_LOG_FILE"

# --- Geocodes ---

# | POST   | /api/geocode/address                    | No   | None  | 
# | POST   | /api/geocode/addresses                  | No   | None  |

# Addresses Test 1 - Create / Decode address

echo "Geocoding new address ..." | tee -a "$TEST_LOG_FILE"
# FIXME: Why do we use a multipart form POST here?
TEST_ADDRESS_A="Alexanderplatz 1, 12000 Berlin"
ADDRESS_POST_RESPONSE=$(http --check-status -f POST "localhost:8081/api/geocode/address" address="${TEST_ADDRESS_A}")
echo "$ADDRESS_POST_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Addresses Test 2 - Create / Decode multiple addresses

echo "Geocoding addresses from CSV file ..." | tee -a "$TEST_LOG_FILE"
# FIXME: Why do we use a multipart form POST here?
TEST_ADDRESS_FILE="http/testdata/input/addresses.csv"
ADDRESSES_POST_RESPONSE=$(http --check-status -f POST "localhost:8081/api/geocode/addresses" file@${TEST_ADDRESS_FILE})
echo "$ADDRESSES_POST_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# --- Groups ---

# | GET    | /api/groups                             | No   | User  |
# | POST   | /api/groups                             | Yes  | Admin |
# | DELETE | /api/groups/{id_or_slug}                | Yes  | Admin |

# Groups Test 1 - Create group

# G1a 'Tasty-Tacos'

echo "Creating new group ..." | tee -a "$TEST_LOG_FILE"
GROUP_CREATION_RESPONSE=$(cat http/testdata/input/group.json | http --check-status -b POST 'localhost:8081/api/groups')
echo "$GROUP_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
# FIXME! Endpoint should return id and slug
# GROUP_A_ID=$(echo "$GROUP_CREATION_RESPONSE" | jq -r .id)
# GROUP_A_SLUG=$(echo "$GROUP_CREATION_RESPONSE" | jq -r .slug)
GROUP_A_SLUG="tasty-tacos"
echo "... Done. Group created with id $GROUP_A_SLUG" | tee -a "$TEST_LOG_FILE"

# G1b 'Superfoods'

echo "Creating new group ..." | tee -a "$TEST_LOG_FILE"
GROUP_CREATION_RESPONSE=$(cat testdata/input/group_superfood.json | http --check-status -b POST 'localhost:8081/api/groups')
echo "$GROUP_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
# FIXME! Endpoint should return id and slug
# GROUP_B_ID=$(echo "$GROUP_CREATION_RESPONSE" | jq -r .id)
# GROUP__B_SLUG=$(echo "$GROUP_CREATION_RESPONSE" | jq -r .slug)
GROUP_B_SLUG="superfood"
echo "... Done. Group created with id $GROUP_B_SLUG" | tee -a "$TEST_LOG_FILE"

# Groups Test 2 - Retrieve groups list

echo "Retrieving groups ..." | tee -a "$TEST_LOG_FILE"
GROUP_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/groups")
echo "$GROUP_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Groups Test 3 - Delete group by slug

# G3a DELETE

echo "Deleting group $GROUP_A_SLUG ..." | tee -a "$TEST_LOG_FILE"
GROUP_DELETE_RESPONSE=$(http --check-status -b DELETE "localhost:8081/api/groups/$GROUP_A_SLUG")
echo "$GROUP_DELETE_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# G3b GET results

echo "Retrieving groups ..." | tee -a "$TEST_LOG_FILE"
GROUP_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/groups")
echo "$GROUP_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"


# --- Contacts / Users ---

# | GET    | /api/contacts                           | Yes  | Admin |
# | POST   | /api/contacts                           | Yes  | User  |
# | GET    | /api/groups/{id_or_slug}/contacts       | Yes  | User  |

# | GET    | /api/users/{contact_id}/                | Yes  | User  |
# | GET    | /api/users/{contact_id}/contacts        | Yes  | User  |

# Contacts Test 1 - Create contact

# C1a Create contact without group

echo "Creating new contact ..." | tee -a "$TEST_LOG_FILE"
CONTACT_CREATION_RESPONSE=$(cat testdata/input/contact_ben.json | http --check-status -b POST 'localhost:8081/api/contacts')
echo "$CONTACT_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
# FIXME! Endpoint should return id or slug
CONTACT_A_ID=1
echo "... Done. Contact created with id $CONTACT_A_ID" | tee -a "$TEST_LOG_FILE"

# C1b Create contact with group

echo "Creating new contact with group ..." | tee -a "$TEST_LOG_FILE"
# FIXME! server can't find group by slug in this context, only by name
# FIXME! Endpoint should return id or slug
CONTACT_CREATION_RESPONSE=$(cat http/testdata/input/contactaddresses-withgroup.json | http --check-status -b POST 'localhost:8081/api/contacts')
echo "$CONTACT_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
# FIXME! Shouldn't contacts have some form of id or slug?
CONTACT_B_ID=2
echo "... Done. Contact created with id $CONTACT_B_ID" | tee -a "$TEST_LOG_FILE"

# Contacts Test 2 - Retrieve contacts list

echo "Retrieving contacts ..." | tee -a "$TEST_LOG_FILE"
# FIXME! Contact ids should be shown in response
CONTACT_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/contacts")
echo "$CONTACT_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Contacts Test 3 - Retrieve group contacts list

echo "Retrieving contacts in group $GROUP_B_SLUG ..." | tee -a "$TEST_LOG_FILE"
# FIXME! Contact ids should be shown in response
CONTACT_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/groups/$GROUP_B_SLUG/contacts")
echo "$CONTACT_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Contacts Test 4 - Retrieve contact / user by id

echo "Retrieving details of user $CONTACT_A_ID ..." | tee -a "$TEST_LOG_FILE"
USER_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/users/$CONTACT_A_ID")
echo "$USER_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Contacts Test 5 - Retrieve contacts list of user by id

echo "Retrieving contact list of user $CONTACT_A_ID ..." | tee -a "$TEST_LOG_FILE"
CONTACT_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/users/$CONTACT_A_ID/contacts")
echo "$CONTACT_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"


# --- Tours ---

# | POST   | /api/tours                              | Yes  | Admin |
# | GET    | /api/tours/{id_or_slug}                 | Yes  | Admin |
# | DELETE | /api/tours/{id_or_slug}                 | Yes  | Admin |

# | GET    | /api/users/{contact_id}/tours           | Yes  | User  |

# FIXME: missing from docs:
# | GET    | /api/tours                              | Yes  | Admin |

# Tours Test 1 - Create tour

# T1a - Tasty Tacos tour

echo "Creating new tour ..." | tee -a "$TEST_LOG_FILE"
TOUR_CREATION_RESPONSE=$(cat http/testdata/input/tour.json | http --check-status -b POST 'localhost:8081/api/tours')
echo "$TOUR_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
# FIXME! Endpoint should return id
# FIXME! API should use unified language - wht is the slug under '.detail'?
TOUR_A_SLUG=$(echo "$TOUR_CREATION_RESPONSE" | jq -r .detail)
echo "... Done. Tour created with slug $TOUR_A_SLUG" | tee -a "$TEST_LOG_FILE"

# T1b - Northern Tour

echo "Creating new tour ..." | tee -a "$TEST_LOG_FILE"
TOUR_CREATION_RESPONSE=$(cat testdata/input/tour_northern.json | http --check-status -b POST 'localhost:8081/api/tours')
echo "$TOUR_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
# FIXME! Endpoint should return id
# FIXME! API should use unified language - wht is the slug under '.detail'?
TOUR_B_SLUG=$(echo "$TOUR_CREATION_RESPONSE" | jq -r .detail)
echo "... Done. Tour created with slug $TOUR_B_SLUG" | tee -a "$TEST_LOG_FILE"

# Tours Test 2 - Retrieve tour

# T2a - Get all tours

echo "Retrieving tours ..." | tee -a "$TEST_LOG_FILE"
TOUR_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/tours")
echo "$TOUR_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# T2b - Get tour by slug

echo "Retrieving tour $TOUR_A_SLUG ..." | tee -a "$TEST_LOG_FILE"
TOUR_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/tours/$TOUR_A_SLUG")
echo "$TOUR_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Tours Test 3 - Delete tour

# T3a DELETE

echo "Deleting tour $TOUR_B_SLUG ..." | tee -a "$TEST_LOG_FILE"
GROUP_DELETE_RESPONSE=$(http --check-status -b DELETE "localhost:8081/api/tours/$TOUR_B_SLUG")
echo "$GROUP_DELETE_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# T3b GET results

echo "Retrieving tours ..." | tee -a "$TEST_LOG_FILE"
TOUR_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/tours")
echo "$TOUR_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Tours Test 4 - Retrieve tour by contact

echo "Retrieving tours of user $CONTACT_A_ID ..." | tee -a "$TEST_LOG_FILE"
TOUR_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/users/$CONTACT_A_ID/tours")
echo "$TOUR_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# --- Tours and items by group ---

# | GET    | /api/groups/{id_or_slug}/tours          | Yes  | User  |
# | POST   | /api/groups/{id_or_slug}/tours          | Yes  | User  |
# | POST   | /api/groups/{id_or_slug}/tours/{id_or_slug}/items           | Yes  | User  |
# | DELETE | /api/groups/{id_or_slug}/tours/{id_or_slug}/items/{item_id} | Yes  | User  |
# | PATCH  | /api/groups/{id_or_slug}/tours/{id_or_slug}/items/{item_id} | Yes  | User  |

# Group Tour Test 1 - Create tour

echo "Creating new tour for group $GROUP_B_SLUG ..." | tee -a "$TEST_LOG_FILE"
TOUR_CREATION_RESPONSE=$(cat testdata/input/tour_northern.json | http --check-status -b POST "localhost:8081/api/groups/$GROUP_B_SLUG/tours")
echo "$TOUR_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
# FIXME! Endpoint should return id
# FIXME! API should use unified language - wht is the slug under '.detail'?
TOUR_C_SLUG=$(echo "$TOUR_CREATION_RESPONSE" | jq -r .detail)
echo "... Done. Tour created with slug $TOUR_C_SLUG" | tee -a "$TEST_LOG_FILE"

# Group Tour Test 2 - Retrieve tours for group

echo "Retrieving tours for group $GROU_B_SLUG ..." | tee -a "$TEST_LOG_FILE"
TOUR_QUERY_RESPONSE=$(http --check-status -b GET "localhost:8081/api/groups/$GROUP_B_SLUG/tours")
echo "$TOUR_QUERY_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done" | tee -a "$TEST_LOG_FILE"

# Group Tour Test 3 - Create tour item for group tour

echo "Creating new item for tour $TOUR_C_SLUG under group $GROUP_B_SLUG ..." | tee -a "$TEST_LOG_FILE"
#TOURITEM_CREATION_RESPONSE=$(cat testdata/input/tour_item_a.json | http --check-status -b POST "localhost:8081/api/groups/$GROUP_B_SLUG/tours/$TOUR_C_SLUG/items")
#echo "$TOURITEM_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
echo "FIXME: This operation crashes the server... feel free to uncomment these two lines to see a spectacle." | tee -a "$TEST_LOG_FILE"
echo "... Done." | tee -a "$TEST_LOG_FILE"

echo "FIXME: Due to the server crash and other undefined behavior, further tests for tour items pending until related issues are resolved." | tee -a "$TEST_LOG_FILE"

# --- Tour Items ---

# | POST   | /api/touritems                 		     | Yes  | Admin |
# | DELETE | /api/touritems/{id}         		         | Yes  | Admin |
# | PATCH  | /api/touritems/{id}         		         | Yes  | Admin |

# --- Vroom ---

# | POST   | /api/vroom                              | No   | None  |
# | POST   | /api/vroom/solver                       | No   | None  |

echo "Sending vroom request ..." | tee -a "$TEST_LOG_FILE"
VROOM_RESPONSE=$(cat http/testdata/input/vroomrequest.json | http --check-status -b POST "localhost:8081/api/vroom")
echo "$VROOM_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done." | tee -a "$TEST_LOG_FILE"

echo "Sending tour to vroom solver ..." | tee -a "$TEST_LOG_FILE"
VROOM_SOLVER_RESPONSE=$(cat http/testdata/input/tour.json | http --check-status -b POST "localhost:8081/api/vroom")
echo "$VROOM_SOLVER_RESPONSE" >> "$TEST_LOG_FILE"
echo "... Done." | tee -a "$TEST_LOG_FILE"

# Done.

SUCCESS=true
