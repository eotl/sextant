package yaml

import (
	"eotl.supply/sextant"
	"eotl.supply/sextant/inmem"
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v3"
)

var _ sextant.ContactAddressDecoder = (*ContactAddressDecoder)(nil)

type ContactAddressDecoder struct {
	parser          *inmem.AddressParser
	dateTimeService *inmem.DateTimeService
	logger          *sextant.Logger
}

func NewContactAddressDecoder(doLog bool) *ContactAddressDecoder {
	parser := inmem.NewAddressParser()
	dateTimeService := inmem.NewDateTimeService()
	logger := &sextant.Logger{doLog, os.Stderr}
	return &ContactAddressDecoder{parser, dateTimeService, logger}
}

func (dec *ContactAddressDecoder) DecodeContactAddresses(filepath string) ([]sextant.ContactAddress, error) {
	if filepath == "" {
		return []sextant.ContactAddress{}, nil
	}
	dec.logger.Printf("Parsing YAML file '%s' ... ", filepath)
	yfile, err := ioutil.ReadFile(filepath)
	if err != nil {
		err = fmt.Errorf("unable to read yaml file %s: %v\n", filepath, err)
		return nil, err
	}
	data := make(map[string]sextant.ContactAddress)
	err = yaml.Unmarshal(yfile, &data)
	if err != nil {
		err = fmt.Errorf("unable to unmarshal yaml data %s: %v\n", filepath, err)
		return nil, err
	}
	contactsUpdated := []sextant.ContactAddress{}
	for _, contact := range data {
		if contact.Address == nil {
			err = fmt.Errorf("\nError: required key 'address' is missing in file %s", filepath)
			return nil, err
		}
		if len(contact.Label) > 0 {
			contact.Label = dec.parser.ParseAddress(contact.Label)
		}
		if contact.Amount < 1 {
			contact.Amount = 1
		}
		contactsUpdated = append(contactsUpdated, contact)
	}
	dec.logger.Printf("%s\n", "done")
	return contactsUpdated, nil
}
