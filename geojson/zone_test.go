package geojson

import (
	_ "fmt"
	_ "os"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	_ "eotl.supply/sextant/helper"
	"github.com/paulmach/orb"
)

func TestZoneDecoder(t *testing.T) {

	t.Run("DecodeZones", func(t *testing.T) {
		decoder := NewZoneDecoder(false)
		wantZone := sextant.Zone{
			Name: "Mitte",
			Geometry: orb.MultiPolygon{
				{{{0, 0}, {0, 2}, {2, 2}, {2, 0}, {0, 0}}},
				{{{1, 1}, {1, 3}, {3, 3}, {3, 1}, {1, 1}}},
			},
		}

		tables := []struct {
			name     string
			input    string
			nameKey  string
			want     []sextant.Zone
			want_err string
		}{
			{"FileOk", "testdata/input/zones.json", "", []sextant.Zone{wantZone}, ""},
			{"FileOkCustomNameKey", "testdata/input/zones-custom-name-key.json", "myname", []sextant.Zone{wantZone}, ""},
			{"WrongNameKey", "testdata/input/zones.json", "wrong-name-key", nil, "No value for namekey 'wrong-name-key' found"},
			{"InvalidNameType", "testdata/input/zones-invalid-name-type.json", "", nil, "Value for namekey 'name' is not a string: 12345"},
		}

		for _, table := range tables {
			t.Run(table.name, func(t *testing.T) {
				got, got_err := decoder.DecodeZones(table.input, table.nameKey)
				if got_err != nil {
					if got_err.Error() != table.want_err {
						t.Errorf("DecodeZones(%s) -> got %+v want %+v", table.input, got_err, table.want_err)
					}
				} else {
					if !reflect.DeepEqual(got, table.want) {
						t.Errorf("DecodeZones(%s) -> got %#v want %#v", table.input, got, table.want)
					}
				}
			})
		}
	})

}
