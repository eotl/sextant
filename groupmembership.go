package sextant

import (
	"context"
	"time"
)

type GroupMembership struct {
	ID               int
	GroupID          int
	ContactAddressID int
	CreatedAt        time.Time
	UpdatedAt        time.Time
}

type GroupMembershipFilter struct {
	ID               *int
	GroupID          *int
	ContactAddressID *int
	Offset           int
	Limit            int
}

func (g *GroupMembership) Validate() error {
	return nil
}

type GroupMembershipService interface {
	FindGroupMembershipByIDs(ctx context.Context, groupID int, contactAddressID int) (*GroupMembership, error)

	FindOrCreateGroupMembership(ctx context.Context, membership *GroupMembership) error
}
